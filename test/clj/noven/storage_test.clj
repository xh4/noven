(ns noven.storage-test
  (:require [noven.storage :refer :all]
            [noven.resources.file :refer :all]
            [noven.fixtures :refer :all]
            [noven.test-helper :refer :all]
            [clojure.test :refer :all])
  (:import [java.awt.image BufferedImageOp]
           [javax.imageio ImageIO]
           [javaxt.io Image]
           [java.io File])(:import [java.io File]))

(use-fixtures :each setup-site)

(def sample-file-map
  {:file test-image})

(deftest test-create-file
  (let [file (add-file sample-file-map)]
    (is (map? file))))

(deftest test-get-file
  (let [returned (add-file sample-file-map)
        file-record (find-file-by-id (:id returned))
        file (get-file file-record)]
    (is (= (class file) java.io.File))
    (is (.exists file))
    (is (.getName file) "test.jpg")))

(deftest test-resize-image
  (let [file-record (add-file sample-file-map)
        image-file (get-file file-record)
        resized-file (resize-image image-file 300)
        resized-image (Image. resized-file)]
    (is (= (max (.getWidth resized-image)
                (.getHeight resized-image))
           300))))

(deftest test-make-version
  )
