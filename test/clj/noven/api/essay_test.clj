(ns noven.api.essay-test
  (:require [noven.fixtures :refer :all]
            [noven.test-helper :refer :all]
            [clojure.test :refer :all]))

(use-fixtures :each setup-site)

(deftest test-create-essay
  (with-response
    (request :post "/api/essays")
    (assert-redirected-to "/api/essays/1")))

(deftest test-get-essay
  (request :post "/api/essays")
  (with-response
    (request :get "/api/essays/1")
    (assert-status :ok)
    (with-body essay
      (are [attr] (nil? (attr essay))
           :title
           :excerpt
           :document
           :draft)))
  (request :post "/api/essays")
  (with-response
    (request :get "/api/essays")
    (assert-status :ok)
    (with-body essays
      (is (seq? essays))
      (is (= (count essays) 2)))))

(deftest test-update-essay
  (request :post "/api/essays")
  (with-response
    (request :put "/api/essays/1" {:title "foo"
                                   :excerpt "bar"
                                   :document "[]"
                                   :draft "[]"})
    (assert-status :ok)
    (with-body essay
      (are [attr value] (= (attr essay) value)
           :title "foo"
           :excerpt "bar"
           :document []
           :draft []))))

(deftest test-delete-essay
  (request :post "/api/essays")
  (with-response
    (request :delete "/api/essays/1")
    (assert-status :ok))
  (with-response
    (request :get "/api/essays/1")
    (assert-status :not-found))
  (with-response
    (request :get "/api/essays")
    (with-body essays
      (is (seq? essays))
      (is (empty? essays)))))

(deftest test-publish-essay
  (request :post "/api/essays")
  ;; publish
  (request :post "/api/essays/1/publish")
  (with-response
    (request :get "/api/essays/1")
    (assert-status :ok)
    (with-body essay
      (is (map? (:content essay)))
      (is (string? (:published_at essay)))))
  ;; unpublish
  (request :post "/api/essays/1/unpublish")
  (with-response
    (request :get "/api/essays/1")
    (assert-status :ok)
    (with-body essay
      (is (nil? (:content essay))))))
