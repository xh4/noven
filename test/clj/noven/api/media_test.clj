(ns noven.api.media-test
  (:require [noven.fixtures :refer :all]
            [noven.test-helper :refer :all]
            [clojure.test :refer :all]))

(use-fixtures :each setup-site)

(defn create-media-entity []
  (request :post "/api/media" {"file" test-image}))

(deftest test-create-media
  (with-response
    (create-media-entity)
    (assert-redirected-to "/api/media/1"))

  (with-response
    (create-media-entity)
    (assert-redirected-to "/api/media/2")))

(deftest test-get-media []
  (create-media-entity)
  (create-media-entity)

  (with-response
    (request :get "/api/media/1")
    (assert-status :ok))

  (with-response
    (request :get "/api/media/2")
    (assert-status :ok)
    (with-body media-entity
      (is (map? media-entity))))

  (with-response
    (request :get "/api/media")
    (assert-status :ok)
    (with-body media
      (is (seq? media))
      (is (= (count media) 2)))))

(deftest test-update-media
  (create-media-entity)
  (with-response
    (request :put "/api/media/1"
                  {:title "foo"
                   :caption "bar"})
    (assert-status :ok)
    (with-body media-entity
      (is (= (:title media-entity) "foo"))
      (is (= (:caption media-entity) "bar")))))

(deftest test-delete-media
  (create-media-entity)
  (with-response
    (request :delete "/api/media/1")
    (assert-status :ok))
  (with-response
    (request :get "/api/media/1")
    (assert-status :not-found))
  (with-response
    (request :get "/api/media")
    (with-body essays
      (is (seq? essays))
      (is (empty? essays)))))

(deftest test-publish-media
  (create-media-entity)
  ;; publish
  (request :post "/api/media/1/publish")
  (with-response
    (request :get "/api/media/1")
    (assert-status :ok)
    (with-body media-entity
      (is (map? (:content media-entity)))
      (is (string? (:published_at media-entity)))))
  ;; unpublish
  (request :post "/api/media/1/unpublish")
  (with-response
    (request :get "/api/media/1")
    (assert-status :ok)
    (with-body media-entity
      (is (nil? (:content media-entity))))))
