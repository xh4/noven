(ns noven.api.storage-test
  (:require [noven.storage :refer :all]
            [noven.fixtures :refer :all]
            [noven.assertions :refer :all]
            [noven.test-helper :refer [setup-site]]
            [clojure.test :refer :all]))

(use-fixtures :each setup-site)
