(ns noven.resources.file-test
  (:require [noven.resources.file :refer :all]
            [noven.fixtures :refer :all]
            [noven.test-helper :refer [setup-site]]
            [clojure.test :refer :all]))

(use-fixtures :each setup-site)

(def sample-file-record
  {:name "test.jpg"
   :size 10000
   :type "image/jpeg"
   :parent_id nil
   :version "original"
   :tag "test-tag"
   :hash "hash"
   :timestamp 1000000000
   :metadata {:width 800
              :height 600}})

(deftest test-create-file
  (let [file (create-file sample-file-record)]
    (is (map? file))
    (is (number? (:id file)))))

(deftest test-find-file
  ;; single
  (create-file sample-file-record)
  (let [file (find-file-by-id 1)]
    (is (map? file))
    (is (= (:id file) 1)))

  ;; all files
  (create-file sample-file-record)
  (create-file (merge sample-file-record {:parent_id 1
                                          :version "small"}))
  (let [files (find-files-by-id 1)]
    (doseq [file files]
      (is (or (= (:id file) 1)
              (= (:parent_id file) 1)))))

  ;; version
  (let [file (create-file (merge sample-file-record {:version "test-version"}))
        returned-file (find-file-versions-by-id (:id file) (:version file))]
    (is (= (:id returned-file) (:id file)))
    (is (= (:version returned-file) (:version file))))

  ;; tag
  (create-file sample-file-record)
  (let [files (find-files-by-tag "test-tag")]
    (doseq [file files]
      (is (= (:tag file) "test-tag")))))

(deftest test-make-url
  (create-file sample-file-record)
  (let [file (find-file-by-id 1)
        url (make-url file "small")]
    (is (= url "/storage/1/test.jpg?version=small"))))
