# Noven

![Library](https://gitlab.com/xh4/noven/raw/master/screenshots/library.png)

## Installation

1. Make sure you have a Java JDK version 7 or later.
2. Install [Leiningen](http://leiningen.org/).

## Development

### Install dependency
```sh
mvn install:install-file \
    -Dfile=lib/javaxt-core-1.7.2.jar \
    -DgroupId=local \
    -DartifactId=javaxt-core \
    -Dversion=1.7.2 \
    -Dpackaging=jar \
    -DgeneratePom=true \
    -DcreateChecksum=true\
    -DlocalRepositoryPath=lib
```
### Run server
```sh
lein run -- server --port 4000 ~/Site
```

### Thanks
Thanks to [魏玄颖](http://yzweixy1029.pp.163.com/) for providing the photos used in screenshots.

## License
The source code is licensed under the GPLv3 (or later) from the Free Software Foundation. A copy of the license is included with every copy of this program, but you can also read the text of the license [here](/LICENSE).
