(defproject noven "0.1.0-SNAPSHOT"

  :url "http://noven.io"

  :source-paths ["src/clj"]

  :test-paths ["test/clj"]

  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [ring "1.3.2"]
                 [ring/ring-json "0.3.1"]
                 [http-kit "2.1.19"]
                 [compojure "1.3.4"]
                 [enlive "1.1.5"]
                 [hiccup "1.0.5"]
                 [liberator "0.12.2"]
                 [lib-noir "0.8.6"]
                 [buddy "0.5.1"]
                 [com.taoensso/timbre "3.4.0"]
                 [org.xerial/sqlite-jdbc "3.8.7"]
                 [korma "0.4.2"]
                 [clj-time "0.8.0"]
                 [cheshire "5.3.1"]
                 [commons-io/commons-io "2.4"]
                 [org.apache.commons/commons-compress "1.8"]
                 [me.raynes/fs "1.4.4"]
                 [org.clojure/tools.cli "0.3.1"]
                 [pandect "0.4.0"]
                 [clj-http "1.0.0"]
                 [kevin1999/ring-mock "0.2.0"]
                 [local/javaxt-core "1.7.2"]
                 [org.twitter4j/twitter4j-core "4.0.3"]
                 [com.twitter/twitter-text "1.6.1"]
                 [slingshot "0.11.0"]
                 [environ "1.0.0"]
                 [org.clojure/tools.namespace "0.2.10"]

                 [org.clojure/clojurescript "0.0-2760"]
                 [org.omcljs/om "0.9.0"]
                 [prismatic/om-tools "0.3.10"]
                 [cljs-ajax "0.3.13"]
                 [secretary "1.2.1"]
                 [domina "1.0.2"]]

  :repositories [["local" ~(str (.toURI (java.io.File. "lib")))]]

  :plugins [[lein-cljsbuild "1.0.6"]
            [lein-environ "1.0.0"]]

  :min-lein-version "2.5.2"

  :cljsbuild {:builds {:dashboard {:source-paths ["src/cljs"]}}}

  :profiles {:dev {:source-paths ["env/dev/clj"]

                   :dependencies [[figwheel "0.2.1-SNAPSHOT"]
                                  [figwheel-sidecar "0.2.1-SNAPSHOT"]
                                  [com.cemerick/piggieback "0.1.4"]
                                  [weasel "0.5.0"]
                                  [org.clojure/tools.nrepl "0.2.10"]]

                   :main ^:skip-aot noven.dev

                   :plugins [[lein-figwheel "0.2.1-SNAPSHOT"]
                             [cider/cider-nrepl "0.9.0"]]

                   :repl-options {:init-ns noven.dev
                                  :timeout 200000
                                  :nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}

                   :env {:environment "development"}

                   :cljsbuild {:builds {:dashboard {:source-paths ["env/dev/cljs"]

                                                    :compiler {:output-to "resources/assets/javascripts/dev/dashboard.js"
                                                               :output-dir "resources/assets/javascripts/dev/out"
                                                               :source-map "resources/assets/javascripts/dev/dashboard.js.map"
                                                               :optimizations :none
                                                               :pretty-print true}}}}}

             :test {:main ^:skip-aot noven.test

                    :env {:environment "test"}

                    :plugins [[com.cemerick/clojurescript.test "0.3.3"]]

                    :cljsbuild {:builds {:dashboard {:source-paths ["env/test/cljs" "test/cljs"]

                                                     :compiler {:output-to "resources/assets/javascripts/test/dashboard.js"
                                                                :output-dir "resources/assets/javascripts/test/out"
                                                                :source-map "resources/assets/javascripts/test/dashboard.js.map"
                                                                :preamble ["react/react.js"]
                                                                :optimizations :whitespace
                                                                :pretty-print true}}}}}

             :uberjar {:source-paths ["env/prod/clj"]
                       :hooks [leiningen.cljsbuild]
                       :env {:environment "production"}
                       :main ^:skip-aot noven.prod
                       :omit-source true
                       :aot :all
                       :cljsbuild {:builds {:dashboard {:source-paths ["env/prod/cljs"]
                                                        :compiler {:output-to "resources/assets/javascripts/prod/dashboard.js"
                                                                   :output-dir "resources/assets/javascripts/prod/out"
                                                                   :preamble ["react/react.min.js"]
                                                                   :optimizations :advanced
                                                                   :pretty-print false}}}}}})
