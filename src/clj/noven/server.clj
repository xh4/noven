(ns noven.server
  (:require [noven.env :as env]
            [noven.storage :refer [storage-routes]]
            [noven.assets :refer [assets-routes]]
            [noven.dashboard :refer [dashboard-routes]]
            [noven.theme :refer [theme-routes]]
            [noven.api :refer [api-routes]]
            [org.httpkit.server :refer [run-server]]
            [liberator.dev :refer [wrap-trace]]
            [noir.util.middleware :refer [app-handler]]
            [taoensso.timbre :as timbre])
  (:use [ring.middleware params
         keyword-params
         nested-params
         multipart-params
         stacktrace
         reload]
        [ring.middleware.json :only [wrap-json-response
                                     wrap-json-params]]))

(timbre/refer-timbre)

(def routes (app-handler [api-routes
                          assets-routes
                          storage-routes
                          dashboard-routes
                          theme-routes]))

(def handler
  (if (env/development?)
    (-> #'routes
        (wrap-trace :header :ui)
        wrap-stacktrace
        wrap-reload
        wrap-nested-params
        wrap-keyword-params
        wrap-multipart-params
        wrap-json-params
        wrap-params
        wrap-json-response)
    (-> routes
        wrap-nested-params
        wrap-keyword-params
        wrap-multipart-params
        wrap-json-params
        wrap-params
        wrap-json-response)))

(defonce instance (atom nil))

(defn start [& [port]]
  (let [port (Integer. (or port 4000))]
    (info "Starting web server on port" port)
    (reset! instance (run-server #'handler {:port port :join? false}))))

(defn stop []
  (when-not (nil? @instance)
    ;; graceful shutdown: wait 100ms for existing requests to be finished
    ;; :timeout is optional, when no timeout, stop immediately
    (@instance :timeout 100)
    (reset! instance nil)))
