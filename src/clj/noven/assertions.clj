(ns noven.assertions
  (:require [clojure.test :refer :all]
            [cheshire.core :as json]))

(defn status-code [status]
  (condp = status
    :ok 200
    :created 201
    :accepted 202
    :non-authoritative-information 203
    :no-content 204
    :reset-content 205
    :partial-content 206
    :multi-status 207
    :im-used 226
    :moved-permanently 301
    :see-other 303
    :temporary-redirect 307
    :bad-request 400
    :unauthorized 401
    :forbidden 403
    :not-found 404
    :method-not-allowed 405
    :not-acceptable 406
    :request-timeout 408
    :conflict 409
    :internal-server-error 500))

(defmacro assert-response-status [response status]
  `(is (= (:status ~response) (if (integer? ~status)
                                ~status
                                (status-code ~status)))))

(defmacro assert-redirected-to [response uri]
  `(are [x# y#] (= x# y#)
        (:status ~response) 303
        (get (:headers ~response) "Location") ~uri))

(defmacro assert-json-response [response]
  `(do
     (is (or (= (get (:headers ~response) "Content-Type") "application/json")
             (= (get (:headers ~response) "Content-Type") "application/json;charset=UTF-8")))
     (is (contains? #{\{ \[} (first (:body ~response))))
     (is (contains? #{\} \]} (last (:body ~response))))))
