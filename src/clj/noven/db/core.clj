(ns noven.db.core
  (:require [noven.env :as env]
            [cheshire.core :as json])
  (:use [korma.core]
        [korma.db]))

(declare files)
(declare media)
(declare essays)
(declare categories)
(declare content-categories)
(declare tags)
(declare content-tags)
(declare contents)
(declare site)
(declare tweets)
(declare text-posts)

(defn initialize []
  (def db (sqlite3 {:db (str env/*path* "/database.db")}))

  (defentity files
    (table :files)
    (database db)
    (transform (fn [record]
                 (let [json-columns #{:metadata}]
                   (reduce-kv (fn [m k v]
                                (if (contains? json-columns k)
                                  (assoc m k (json/parse-string v))
                                  (assoc m k v))) {} record)))))

  (defentity media
    (table :media)
    (database db)
    (transform (fn [record]
                 (let [boolean-columns #{:trash}]
                   (reduce-kv (fn [m k v]
                                (if (contains? boolean-columns k)
                                  (assoc m k (condp = v
                                               1 true
                                               0 false
                                               nil))
                                  (assoc m k v))) {} record)))))

  (defentity essays
    (table :essays)
    (database db))

  (defentity categories
    (table :categories)
    (database db))

  (defentity content-categories
    (table :content_categories)
    (database db))

  (defentity tags
    (table :tags)
    (database db))

  (defentity content-tags
    (table :content_tags)
    (database db))

  (defentity contents
    (table :contents)
    (database db))

  (defentity site
    (table :site)
    (database db))

  (defentity tweets
    (table :tweets)
    (database db))

  (defentity text-posts
    (table :text_posts)
    (database db)))
