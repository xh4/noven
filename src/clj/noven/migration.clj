(ns noven.migration
  (:require [noven.env :as env]
            [noven.util :as util]
            [clojure.java.io :as io]
            [me.raynes.fs :as fs]
            [cheshire.core :as json]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clj-time.local :as l]
            [clojure.tools.namespace.find :as ns])
  (:use [clojure.java.jdbc])
  (:import [java.io File]
           [java.util.zip ZipEntry ZipOutputStream]
           [org.apache.commons.compress.archivers.zip ZipFile]
           [org.apache.commons.io IOUtils]))

(defn get-db [] {:classname "org.sqlite.JDBC"
                 :subprotocol "sqlite"
                 :subname (str env/*path* "/database.db")})

(defn valid-database? [path]
  (let [db-spec {:classname "org.sqlite.JDBC"
                 :subprotocol "sqlite"
                 :subname path}]
    (try
      (query db-spec "pragma schema_version")
      true
      (catch Exception e
        false))))

(defn table-exists? [name]
  (with-db-metadata [meta (get-db)]
    (let [res (.getTables meta nil nil name (into-array String ["TABLE"]))]
      (.next res))))

(defn create-schema-table []
  (db-do-commands (get-db)
    (create-table-ddl :schema_migrations
      [:id "integer" "primary key"]
      [:version "integer" "not null"])))

(defn ensure-schema-table []
  (when-not (table-exists? "schema_migrations")
    (create-schema-table)))

(defn schema-version []
  (ensure-schema-table)
  (let [res (query (get-db)
                   ["select version from schema_migrations"])]
    (or (:version (last res)) 0)))

(defn update-schema-version [version]
  (insert! (get-db) "schema_migrations" {:version version}))

(defn create-tables [tables]
  (let [tables (remove #(table-exists? (:name %)) tables)
        ddls (map #(apply create-table-ddl (:name %) (:columns %)) tables)]
    (when-not (empty? ddls)
      (apply db-do-commands (get-db) ddls))))

(defn drop-tables [tables]
  (let [tables (filter #(table-exists? (:name %)) tables)
        ddls (map #(drop-table-ddl (:name %)) tables)]
    (when-not (empty? ddls)
      (apply db-do-commands (get-db) ddls))))

(defn make-backup-name []
  (f/unparse (f/formatter "yyyy-MM-dd HH.mm.ss") (l/local-now)))

(defn backup-site []
  (let [filename (make-backup-name)]
    (println (str "Backing up site to file \"" filename ".zip\"..."))
    (fs/mkdirs (str env/*path* "/Backups"))
    (let [filename (make-backup-name)
          tmpfile (File/createTempFile filename ".tmp")
          source-path env/*path*
          target-file (io/file (str env/*path* "/Backups/" filename ".zip"))]
      (with-open [zip (ZipOutputStream. (io/output-stream tmpfile))]
        (doseq [f (file-seq (io/file source-path)) :when (.isFile f)]
          (let [path (subs (.getPath f) (.length source-path))]
            (when-not (.startsWith path "/Backups")
              (.putNextEntry zip (ZipEntry. path))
              (io/copy f zip)
              (.closeEntry zip)))))
      (io/copy tmpfile target-file)
      target-file)))

(defn restore-site [backup-file]
  (println (str "Restoring site from backup file \"" (.getName backup-file) "\"..."))
  (with-open [zip (ZipFile. backup-file)]
    (doseq [entry (enumeration-seq (.getEntries zip))]
      (let [in-stream (.getInputStream zip entry)
            local-file (io/file (str env/*path* (.getName entry)))]
        (io/make-parents local-file)
        (with-open [out-stream (io/output-stream local-file)]
          (io/copy in-stream out-stream))))))

(def ns-regex #"^noven\.migrations\.m(\d{14})$")

(defn all-migrations []
  (->> (all-ns)
       (map ns-name)
       (map str)
       (filter #(re-find ns-regex %))))

(defn run-migration [version fn]
  (println "Running migration" version "...")
  (try
    (fn)
    (println "Running migration" version "SUCCESS")
    (catch Exception e
      (println "Running migration" version "FAIL")
      (throw e))))

(defmacro defmigration [version & body]
  (if (string? (first body))
    `(defn migrate
       ~(first body)
       []
       (run-migration ~version (fn [] ~@(rest body))))
    `(defn migrate []
       (run-migration ~version (fn [] ~@(rest body))))))

(defn run-migrations []
  (let [current-version (schema-version)
        namespaces (all-migrations)
        versions (map #(Long. (second (re-find ns-regex %))) namespaces)
        ahead (sort (filter #(> % current-version) versions))]
    (when-not (empty? ahead)
      (let [backup-file (backup-site)]
        (try
          (doseq [version ahead]
            (let [migrate-fn (-> (str "noven.migrations.m" version "/migrate")
                                 symbol
                                 resolve)]
              (run-migration version migrate-fn)))
          (update-schema-version (last ahead))
          (catch Exception e
            (restore-site backup-file)))))))
