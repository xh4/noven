(ns noven.dashboard
  (:require [noven.env :as env]
            [hiccup.page :refer [html5]]
            [compojure.core :refer [defroutes GET]])
  (:use [hiccup.core]))

(defn render []
  (html5
   [:head
    [:meta {:http-equiv "Content-Type" :content "text/html; charset=utf-8"}]
    [:meta {:name "viewport"
            :content "width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1"}]

    (if (env/development?)
      (list
       [:link {:type "text/css" :href "/assets/dev/dashboard.css" :rel "stylesheet"}]
       [:script {:type "text/javascript" :src "/assets/vendor/react.js"}]
       [:script {:type "text/javascript" :src "/assets/dev/out/goog/base.js"}]))

    (if (env/production?)
      (list
       [:link {:type "text/css" :href "/assets/prod/dashboard.css" :rel "stylesheet"}]))

    [:script {:type "text/javascript" :src "/assets/ace-min-noconflict/ace.js"}]

    [:title "Noven"]]

   [:body
    [:div#container]

    (if (env/development?)
      (list
       [:script {:type "text/javascript" :src "/assets/dev/dashboard.js"}]
       [:script {:type "text/javascript"}
        "goog.require('noven.dev')"]))

    (if (env/production?)
      [:script {:type "text/javascript" :src "/assets/prod/dashboard.js"}])]))

(defroutes dashboard-routes
  (GET "/dashboard" [] (render)))
