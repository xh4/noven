(ns noven.theme
  (:require [noven.env :as env]
            [noven.util :as util]
            [noven.assets :refer [asset-path]]
            [noven.storage :as storage]
            [noven.resource :refer [api-resource]]
            [noven.resources.file :refer :all]
            [noven.db.core :as db]
            [hiccup.page :refer [html5]]
            [hiccup.element :refer [javascript-tag]]
            [cheshire.core :as json]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [taoensso.timbre :as timbre]
            [compojure.core :refer [defroutes GET]])
  (:import [org.joda.time.format ISODateTimeFormat])
  (:use [korma.core]))

(timbre/refer-timbre)

(defn classname [& classes]
  (->> classes
       (interpose " ")
       (apply str)))

(defn format-date [s]
  (let [y-m-d (rest (re-find #"(\d\d\d\d)-(\d\d)-(\d\d)" s))
        month-map {"01" "January"
                   "02" "February"
                   "03" "March"
                   "04" "April"
                   "05" "May"
                   "06" "June"
                   "07" "July"
                   "08" "August"
                   "09" "September"
                   "10" "October"
                   "11" "November"
                   "12" "December"}]
    (str (get month-map (second y-m-d)) " " (last y-m-d) ", " (first y-m-d))))

(defn combine-list-items [blocks]
  (reduce (fn [col itm]
            (if (= (:type itm) "list-item")
              (if (= (:type (last col)) "list")
                (conj (vec (butlast col)) (update-in (last col) [:items] conj itm))
                (conj col {:type "list"
                           :items [itm]}))
              (conj col itm))) [] blocks))

(defn header [page]
  [:header.top
   [:h1.site-title
    [:a {:href "/" :title "Home"} "Noven"]]
   [:nav#main-nav
    [:ul
     [:li [:a {:title "Home" :href "/" :class (if (= page :home) "k-nav-current")} "Home"]]
     [:li [:a {:title "Timeline" :href "/timeline" :class (if (= page :timeline) "k-nav-current")} "Timeline"]]
     [:li [:a {:title "Contents" :href "/contents" :class (if (= page :contents) "k-nav-current")} "Contents"]]
     [:li [:a {:title "About" :href "/about" :class (if (= page :about) "k-nav-current")} "About"]]]]])

(def footer
  [:footer.bot
   [:nav {:style "display: none;"}
    [:ul
     ;; [:li
     ;;  [:a {:title "Google+" :href "https://plus.google.com/+kevin1999"} "Google+"]]
     ;; [:li
     ;;  [:a {:title "Twitter" :href "https://twitter.com/fying1999" :target "_blank"} "Twitter"]]
     ;; [:li
     ;;  [:a {:title "RSS" :href "#"} "RSS"]]
     ]]
   [:p.copyright "Powered by "
    [:a {:href "http://noven.io"} "Noven"]]])

(defn single-layout []
  [:div.main-content
   [:figure
    [:div.img-wrap
     [:a {:href "#"}
      [:img {:src "/assets/tattoo.jpg"
             :style "max-width:500px;width:75%;"}]]]]])

(defmacro layout-page [page & inner]
  `(html5
    [:head
     [:meta {:charset "utf-8"}]
     [:meta {:http-equiv "X-UA-Compatible" :content "IE=edge,chrome=1"}]
     [:meta {:name "viewport"
             :content "width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1"}]

     [:title "Noven"]

     [:link {:rel "stylesheet" :type "text/css" :href "/assets/vendor/normalize.css"}]
     (if (env/development?)
       [:link {:rel "stylesheet" :type "text/css" :href "/assets/dev/main.css"}])
     (if (env/production?)
       [:link {:rel "stylesheet" :type "text/css" :href "/assets/prod/main.css"}])

     [:link {:rel "shortcut icon" :href "http://luketscharke.com/favicon.ico" :type "image/x-icon"}]

     (javascript-tag (-> (util/resource "april_1st/assets/javascripts/lazy.js")
                         (slurp)))]
    [:body
     [:div#container
      ~(header page)
      [:main
       ~@inner]
      footer]
     [:script {:src "/assets/twitter.js"}]]))

;; (defn media-size [sizes]
;;   (let [larger (filter #(>= (:width %) 500) sizes)
;;         larger (if (empty? larger) (reverse sizes) larger)
;;         size (last sizes)
;;         [width height version] [(:width size) (:height size) (:version size)]
;;         figure-height (if (> height 260)
;;                         260
;;                         height)
;;         image-width (if (> width 500) 500 width)
;;         image-height (if (> width 500)
;;                        (* height (/ image-width width))
;;                        height)xbxb
;;         top (/ (- figure-height image-height) 2.0)
;;         left (/ (- 500 image-width) 2.0)]
;;     {:figure-height figure-height
;;      :top top
;;      :left left
;;      :image-width image-width
;;      :version version
;;      :original-width width
;;      :original-height height}))

(defn media-size [sizes]
  (last sizes))

(defn tweet-item [tweet]
  [:div {:class "tweet"
         :data-id (:id tweet)}
   [:header
    [:time (:time tweet)]]
   [:p (:cooked tweet)]
   (if-let [media (:media tweet)]
     (let [sizes (:sizes media)
           size (media-size sizes)
           file-records (find-files-by-tag (str "tweet:" (:id tweet)))
           file-record (first (filter #(= (:version %) "large") file-records))
           url (make-url file-record (:version size))]
       [:figure {:class "lazy-image"
                 :data-image-height (:height size)
                 :data-image-width (:width size)
                 :data-image-url url
                 :data-lazy "true"}
        [:img {:src url
               :style "display: none;"}]]))])

(defmulti render-block
  (fn [block] (:type block)))

(defmethod render-block "paragraph" [block]
  [:p (:text block)])

(defmethod render-block "list" [block]
  (let [list [:ul]]
    (reduce (fn [ul {:keys [text] :as list-item}]
              (conj ul [:li text])) list (:items block))))

(defmethod render-block "figure" [block]
  ;; (let [media-entity (media-get (:id block))
  ;;       file (:file media-entity)]
  ;;   [:figure
  ;;    [:img {:src (make-url file "m")}]])
  )

(defn combine-list-items [blocks]
  (reduce (fn [col itm]
            (if (= (:type itm) "list-item")
              (if (= (:type (last col)) "list")
                (conj (vec (butlast col)) (update-in (last col) [:items] conj itm))
                (conj col {:type "list"
                           :items [itm]}))
              (conj col itm))) [] blocks))

(defn render-blocks [blocks]
  (let [blocks (combine-list-items blocks)]
    (map render-block blocks)))

(defmulti render-content
  (fn [content] (:type content)))

(defmethod render-content "image" [content]
  (let [media-entity (:detail content)
        file (:file media-entity)]
    [:article {:class (classname "content image")}
     [:header
      [:time (format-date (:created_at content))]
      [:h1
       [:a {:href (make-url file "large")
            :target "_blank"}
        (if-not (empty? (:title media-entity))
          (:title media-entity)
          (:name file))]]]
     [:div.article-content
      [:figure
       [:a {:href (make-url file "large")
            :target "_blank"}
        [:img {:src (make-url file "medium")}]]
       (if-not (empty? (:caption media-entity))
         [:figcaption (:caption media-entity)])]]]))

(defmethod render-content "essay" [content]
  (let [essay (:detail content)]
    [:article {:class (classname "content" "essay")}
     [:header
      [:time (format-date (:created_at content))]
      [:h1
       [:a {:href "#"}
        (:title essay)]]]
     [:div.essay-content
      (render-blocks (:document essay))]]))

(defn empty-state []
  [:p {:class "empty-state"}
   "No contents yet..."])

(defn contents-page []
  (layout-page
   :contents
   [:div {:class "timeline"}
    (let [contents (api-resource "/api/contents")]
      (if (empty? contents)
        (empty-state)
        (for [content contents]
          (render-content content))))]))

(defn make-tweet [record]
  (let [tweet (-> record
                  :content
                  (json/parse-string true))
        time-str (:time tweet)
        time (->> time-str
                  ;; "2014-10-24T11:30:20.000+08:00"
                  (f/parse (ISODateTimeFormat/basicDateTime))
                  (f/unparse (f/formatter "MMM dd, HH:mm")))]
    (assoc tweet :time time)))

(defn timeline-page []
  (layout-page
   :timeline
   [:div {:class "timeline"}
    [:div {:class "timeline-icon"}
     [:img {:src "/assets/twitter-icon.svg"
            :style "height:20px;"}]]
    (let [records (select db/tweets)
          tweets (map make-tweet records)]
      (if (empty? tweets)
        (empty-state)
        (for [tweet tweets]
          (tweet-item tweet))))]))

(defn about-page []
  (layout-page
   :about
   [:div.main-content
    [:p
     "Hello, this is my site."]]))

(defn home-page []
  (layout-page
   :home
   (single-layout)))

(defroutes theme-routes
  (GET "/" [] (home-page))
  (GET "/timeline" [] (timeline-page))
  (GET "/contents" [] (contents-page))
  (GET "/about" [] (about-page)))
