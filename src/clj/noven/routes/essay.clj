(ns noven.routes.essay
  (:require [noven.util :as util]
            [noven.resources.essay :refer :all]
            [noven.resources.content :refer :all]
            [noven.resources.conf :as conf]
            [noven.resources.transformer :as t]
            [noven.resources.validator :as v]
            [noven.resources.sanitizer :as s]
            [liberator.core :refer [defresource]]))

(def CREATE-permitted-params [])
(def CREATE-params-transformer {})
(def CREATE-params-validator {})
(def CREATE-params-sanitizer {})

(def UPDATE-permitted-params [:title :excerpt :document :draft])

(def UPDATE-params-transformer {:title t/STRING
                                :excerpt t/STRING
                                :document t/JSON
                                :draft t/JSON})

(def UPDATE-params-validator {})

(def UPDATE-params-sanitizer {})

(def LIST-permitted-params [])

(def LIST-params-transformer {})

(def LIST-params-validator {})

(def LIST-params-sanitizer {})

(defn assoc-content [essay]
  (let [content (find-content-by-detail {:type "essay"
                                         :detail_id (:id essay)})]
    (assoc essay :content content)))

(defn find-by-id [id]
  (let [essay (find-essay-by-id id)]
    (when essay
      (assoc-content essay))))

(defn find-by-params [params]
  (let [essays (find-essay params)]
    (map assoc-content essays)))

(defn publish [id]
  (update-essay id {:published_at (util/date-time)
                    :state "public"})
  (create-content {:type "essay"
                   :detail_id id}))

(defn unpublish [id]
  (update-essay id {:state "draft"})
  (delete-content-by-detail {:type "essay"
                             :detail_id id}))

(defn delete [id]
  (delete-content-by-detail {:type "essay"
                             :detail_id id})
  (delete-essay id))

(defresource essay [id]
  (merge conf/json
         conf/entry
         conf/authorize
         conf/exception)
  :exists? (fn [ctx]
             (when-let [essay (find-by-id id)]
               true))
  :malformed? (fn [ctx]
                (condp = (get-in ctx [:request :request-method])
                  :get false
                  :put (conf/malformed? ctx {:selector UPDATE-permitted-params
                                             :transformer UPDATE-params-transformer
                                             :validator UPDATE-params-validator})
                  :delete false
                  nil))
  :handle-malformed (fn [ctx] (:errors ctx))
  :handle-ok (fn [ctx] (find-by-id id))
  :put! (fn [ctx]
          (let [params (:params ctx)]
            (update-essay id params)))
  :delete! (fn [ctx]
             (delete id))
  :can-put-to-missing? false
  :new? false
  :respond-with-entity? true)

(defresource essays []
  (merge conf/json
         conf/collection
         conf/exception)
  :handle-ok (fn [ctx] (find-by-params (get-in ctx [:request :params])))
  :malformed? (fn [ctx]
                (condp = (get-in ctx [:request :request-method])
                  :get false
                  :post (conf/malformed? ctx {:selector CREATE-permitted-params
                                              :transformer CREATE-params-transformer
                                              :validator CREATE-params-validator})
                  nil))
  :handle-malformed (fn [ctx] (:errors ctx))
  :post! (fn [ctx] {:essay (create-essay (get-in ctx [:request :params]))})
  :post-redirect? (fn [ctx] {:location (str "/api/essays/" (get-in ctx [:essay :id]))}))

(defresource essay-publish [id]
  (merge conf/json
         conf/exception)
  :allowed-methods [:post]
  :exists? (fn [ctx]
             (when-let [essay (find-by-id id)]
               true))
  :post! (fn [ctx]
           (publish id))
  :can-post-to-missing? false
  :post-redirect? (fn [ctx] {:location (str "/api/essays/" id)}))

(defresource essay-unpublish [id]
  (merge conf/json
         conf/exception)
  :allowed-methods [:post]
  :exists? (fn [ctx]
             (when-let [essay (find-by-id id)]
               true))
  :post! (fn [ctx]
           (unpublish id))
  :can-post-to-missing? false
  :post-redirect? (fn [ctx] {:location (str "/api/essays/" id)}))
