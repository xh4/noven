(ns noven.routes.content
  (:require [noven.resources.content :refer :all]
            [noven.resources.media :refer :all]
            [noven.resources.essay :refer :all]
            [noven.resources.conf :as conf]
            [noven.resources.transformer :as t]
            [noven.resources.validator :as v]
            [noven.resources.sanitizer :as s]
            [liberator.core :refer [defresource]]))

(def CREATE-permitted-params [:detail_id
                              :type])
(def CREATE-params-transformer {:detail_id t/INTEGER
                                :type t/STRING})
(def CREATE-params-validator {})
(def CREATE-params-sanitizer {})

(def LIST-permitted-params [])
(def LIST-params-transformer {})
(def LIST-params-validator {})
(def LIST-params-sanitizer {})

(defn assoc-detail [content]
  (let [detail (condp = (:type content)
                 "image" (find-media-by-id (:detail_id content))
                 "essay" (find-essay-by-id (:detail_id content))
                 nil)]
    (assoc content :detail (apply dissoc detail [:content]))))

(defn find-by-id [id]
  (let [content (find-content-by-id id)]
    (assoc-detail content)))

(defn find-by-params [params]
  (let [contents (find-content params)]
    (map assoc-detail contents)))

(defresource content [id]
  (merge conf/json
         conf/entry
         conf/authorize
         conf/exception)
  :exists? (fn [ctx]
             (when-let [content (find-by-id id)]
               true))
  :handle-ok (fn [ctx] (find-by-id id)))

(defresource contents []
  (merge conf/json
         conf/collection
         conf/exception)
  :handle-ok (fn [ctx]
               (find-by-params (:params ctx)))
  :malformed? (fn [ctx]
                (condp = (get-in ctx [:request :request-method])
                  :get false
                  nil))
  :handle-malformed (fn [ctx] (:errors ctx)))
