(ns noven.routes.media
  (:require [noven.util :as util]
            [noven.resources.media :refer :all]
            [noven.resources.content :refer :all]
            [noven.resources.file :refer :all]
            [noven.resources.conf :as conf]
            [noven.resources.transformer :as t]
            [noven.resources.validator :as v]
            [noven.resources.sanitizer :as s]
            [liberator.core :refer [defresource]]))

(def LIST-permitted-params [:trash
                            ;; :type
                            :last_import
                            :collection_id
                            :visibility
                            :order ;; filename, date, popularity
                            :after
                            :limit])

(def LIST-params-transformer {:trash t/BOOLEAN
                              :last_import t/BOOLEAN
                              :type t/STRING
                              :collection_id t/INTEGER
                              :vasibility t/STRING
                              :order t/STRING
                              :after t/INTEGER
                              :limit t/INTEGER})

(def LIST-params-validator {})

(def LIST-params-sanitizer {:order #(condp = %
                                      "date:ASC" [:created_at :ASC]
                                      "date:DESC" [:created_at :DESC]
                                      [:created_at :DESC])})

(def CREATE-permitted-params [:file
                              :head
                              :collection_id])

(def CREATE-params-transformer {:file t/FILE
                                :head t/BOOLEAN
                                :collection t/INTEGER})

(def CREATE-params-validator {:file v/precence})

(def UPDATE-permitted-params [:title
                              :caption
                              :license
                              :download
                              :collection_id])

(def UPDATE-params-transformer {:title t/STRING
                                :caption t/STRING
                                :license t/STRING
                                :download t/STRING
                                :callection_id t/INTEGER})

(def UPDATE-params-validator {})

;; (defn publish [id]
;;   (let [existed (first (k/select db/contents
;;                                  (where {:detail_id id})))]
;;     (when-not existed
;;       (create-content "image" id))))

;; (defn unpublish [id]
;;   (let [media (find-by-id id)
;;         content-id (get-in media [:content :id])]
;;     (when content-id
;;       (delete-content content-id))))

(defn assoc-content [media-entity]
  (let [content (find-content-by-detail {:detail_id (:id media-entity)
                                         :type "image"})]
    (assoc media-entity :content content)))

(defn find-by-id [id]
  (let [media-entity (find-media-by-id id)]
    (when media-entity
      (assoc-content media-entity))))

(defn find-by-params [params]
  (let [media (find-media params)]
    (map assoc-content media)))

(defn publish [id]
  (update-media id {:published_at (util/date-time)})
  (create-content {:detail_id id
                   :type "image"}))

(defn unpublish [id]
  (delete-content-by-detail {:detail_id id
                             :type "image"}))

(defn delete [id]
  (let [media-entity (find-media-by-id id)
        file-id #(get-in % [:file :id] media-entity)]

    (delete-content-by-detail {:detail_id id
                               :type "image"})
    (delete-media id)
    (delete-file file-id)

    ;; delete cache files
    ;; (let [dir (str "Library/Cache/" (first (storage/path-id file-id)))
    ;;       file-dir (io/file (io/resource dir))]
    ;;   (when file-dir
    ;;     (fs/delete-dir file-dir)))
    ;; delete original file
    ;; (let [original-file (storage/original-file file-id)]
    ;;   (when original-file
    ;;     (fs/delete original-file)))
    ))

(defresource media-entity [id]
  (merge conf/json
         conf/entry
         conf/authorize
         conf/exception)
  :exists? (fn [ctx]
             (when-let [media-entity (find-by-id id)]
               true))
  :malformed? (fn [ctx]
                (condp = (get-in ctx [:request :request-method])
                  :get false
                  :put (conf/malformed? ctx {:selector UPDATE-permitted-params
                                             :transformer UPDATE-params-transformer
                                             :validator UPDATE-params-validator})
                  :delete false
                  nil))
  :handle-malformed (fn [ctx]
                      (:errors ctx))
  :handle-ok (fn [ctx] (find-by-id id))
  :put! (fn [ctx]
          (let [params (:params ctx)]
            (update-media id params)))
  :delete! (fn [ctx]
             (delete id))
  :can-put-to-missing? false
  :new? false
  :respond-with-entity? true)

(defresource media-entities []
  (merge conf/json
         conf/collection
         conf/exception)
  :handle-ok (fn [ctx]
               (find-by-params (:params ctx)))
  :malformed? (fn [ctx]
                (condp = (get-in ctx [:request :request-method])
                  :get (conf/malformed? ctx {:selector LIST-permitted-params
                                             :transformer LIST-params-transformer
                                             :validator LIST-params-validator})
                  :post (conf/malformed? ctx {:selector CREATE-permitted-params
                                              :transformer CREATE-params-transformer
                                              :validator CREATE-params-validator})
                  nil))
  :handle-malformed (fn [ctx] (:errors ctx))
  :post! (fn [ctx] {:media-entity (create-media (:params ctx))})
  :post-redirect? (fn [ctx] {:location (str "/api/media/" (get-in ctx [:media-entity :id]))}))

(defresource media-publish [id]
  (merge conf/json
         conf/exception)
  :allowed-methods [:post]
  :exists? (fn [ctx]
             (when-let [media-entity (find-by-id id)]
               true))
  :post! (fn [ctx]
           (publish id))
  :can-post-to-missing? false
  :post-redirect? (fn [ctx] {:location (str "/api/media/" id)}))

(defresource media-unpublish [id]
  (merge conf/json
         conf/exception)
  :allowed-methods [:post]
  :exists? (fn [ctx]
             (when-let [media-entity (find-by-id id)]
               true))
  :post! (fn [ctx]
           (unpublish id))
  :can-post-to-missing? false
  :post-redirect? (fn [ctx] {:location (str "/api/media/" id)}))
