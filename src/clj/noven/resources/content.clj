(ns noven.resources.content
  (:require [noven.db.core :as db]
            [noven.util :as util]
            [taoensso.timbre :as timbre]
            [korma.core :as k :refer [where values limit
                                      order set-fields]]))

(timbre/refer-timbre)

(defn find-content-by-id
  "Get a content without associating it's detail."
  [id]
  (first (k/select db/contents
                   (where {:id id}))))

(defn find-content-by-detail
  "Get a content by detail's id without associating it's detail,
  detail is like {:type \"image\" :detail_id 1}"
  [detail]
  (first (k/select db/contents
                   (where detail))))

(defn find-content [params]
  (k/select db/contents
            (order :created_at :DESC)))

(defn delete-content-by-detail
  ;; {:type "image" :detail_id 1}
  [detail]
  (k/delete db/contents
            (where detail)))

;; FIXME: move detail existence check to validator
(defn create-content [params]
  (let [detail-id (:detail_id params)
        type (:type params)
        existed (first (k/select db/contents
                                 (where {:type type
                                         :detail_id detail-id})))
        detail (condp = type
                 "image" (first (k/select db/media
                                          (where {:id detail-id})))
                 "essay" (first (k/select db/essays
                                          (where {:id detail-id})))
                 nil)]
    (when (and (not existed)
               detail)
      (let [time (util/date-time)
            record (k/insert db/contents
                             (values {:detail_id detail-id
                                      :type type
                                      :created_at time
                                      :updated_at time}))
            id (get-in record [(keyword "last_insert_rowid()")])]
        (merge record {:id id})))))
