(ns noven.resources.conf
  (:require [noven.resources.transformer :as t]
            [noven.resources.validator :as v]
            [noven.resources.sanitizer :as s])
  (:import java.io.StringWriter
           java.io.PrintWriter))

(def json
  {:available-media-types ["application/json"]})

(def entry
  {:allowed-methods [:get :put :delete]})

(def collection
  {:allowed-methods [:get :post]})

(def authorize
  {:authorized? true})

(def exception
  {:handle-exception
   (fn [{e :exception}]
     (let [sw (StringWriter.)
           pw (PrintWriter. sw true)]
       (.printStackTrace e pw)
       (.. sw (getBuffer) (toString))))})


(defn malformed? [ctx {:keys [selector transformer validator sanitizer]}]
  (let [params (get-in ctx [:request :params])
        selected (select-keys params selector)
        transformed (t/transform-params selected transformer)]
    (if (empty? (:errors transformed))
      (let [errors (v/validate-params (:params transformed) validator)]
        (if-not (empty? errors)
          {:errors errors
           :representation {:media-type "application/json"}}
          [false (let [request (:request ctx)
                       transformed-params (:params transformed)
                       params (if sanitizer
                                (s/sanitize-params transformed-params sanitizer)
                                transformed-params)]
                   {:params params})]))
      {:errors (:errors transformed)
       :representation {:media-type "application/json"}})))
