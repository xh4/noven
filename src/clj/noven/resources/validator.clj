(ns noven.resources.validator
  (:require [slingshot.slingshot :refer [try+ throw+]]))

(defn precence [v]
  (when-not v "Not precence"))

(defn exclusion []
  (fn [data]))

(defn inclusion []
  (fn [data]))

(defn length []
  (fn [data]))

(defn numericality []
  (fn [data]))

(defn validate-params [params validator]
  (reduce
   (fn [errors validator]
     (let [key (first validator)
           value (get params key)
           validate-fn (second validator)]
       (if validate-fn
         (let [err (validate-fn value)]
           (if (empty? err)
             errors
             (merge errors [key err])))
         errors)))
   {}
   validator))
