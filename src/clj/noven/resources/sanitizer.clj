(ns noven.resources.sanitizer)

(defn sanitize-params [params sanitizer]
  (reduce (fn [p pair]
            (let [key (first pair)
                  value (second pair)
                  sanitize-fn (get sanitizer key)]
              (if sanitize-fn
                (let [sanitized (sanitize-fn value)]
                  (merge p [key sanitized]))
                (merge p pair)))) {} params))
