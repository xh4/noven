(ns noven.resources.site
  (:require [noven.util :as util]
            [noven.db.core :as db]
            [noven.resources.conf :as conf]
            [korma.core :as k :refer [where values limit
                                      order set-fields]]
            [liberator.core :refer [defresource]]))

(defn get-by-key [key]
  (let [record (first (k/select db/site
                                (where {:key key})))]
    (if record (:value record))))

(defn get-all []
  (k/select db/site))

(defn delete-key [key]
  (k/delete db/site
            (where {:key key})))

(defn set-kv [key value]
  (let [key (if (keyword? key) (name key) key)]
    (if-let [record (get-by-key key)]
      (k/update db/site
                (set-fields {:value value})
                (where {:key key}))
      (k/insert db/site
                (values {:key key :value value})))))
