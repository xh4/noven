(ns noven.resources.file
  (:require [noven.db.core :as db]
            [noven.util :as util]
            [clojure.java.io :as io]
            [me.raynes.fs :as fs]
            [cheshire.core :as json]
            [clojure.string :as string]
            [korma.core :as k :refer [where values limit
                                      order set-fields]]
            [liberator.core :refer [defresource]])
  (:import javax.imageio.ImageIO))

(defn make-url [file-record & [version]]
  (let [id (:id file-record)
        name (:name file-record)]
    (str "/storage/" id "/" name (if version (str "?version=" version)))))

(defn find-file-by-id [id]
  (first (k/select db/files
                   (where {:id id}))))

(defn find-files-by-id
  "Find file by id including all it's generated versions. "
  [id]
  (k/select db/files
            (where (or (= :id id)
                       (= :parent_id id)))))

(defn find-file-versions-by-id
  [id version]
  (first (k/select db/files
                   (where (or {:id id
                               :version version}
                              {:parent_id id
                               :version version})))))

(defn find-files-by-tag [tag]
  (k/select db/files
            (where {:tag tag})))

(defn create-file [record]
  (let [record (-> record
                   (update-in [:metadata] json/generate-string))
        return (k/insert db/files
                         (values record))
        id (get-in return [(keyword "last_insert_rowid()")])]
    (merge record {:id id})))

(defn delete-file [id]
  (k/select db/files
            (where {:id id})))
