(ns noven.resources.essay
  (:require [noven.util :as util]
            [noven.db.core :as db]
            [ring.util.response :refer [response]]
            [clojure.string :as string]
            [cheshire.core :as json]
            [slingshot.slingshot :refer [try+ throw+]]
            [korma.core :as k :refer [where values limit
                                      order set-fields]]))

(defn parse-document [essay]
  (-> essay
      (update-in [:document] json/parse-string)
      (update-in [:draft] json/parse-string)))

(defmulti validate-block*
  (fn [block]
    (:type block)))

(defmethod validate-block* "paragraph"
  [block]
  )

(defn validate-block [block]
  (if (or (not (map? block))
          (not (contains? block :type)))
    (throw+ "Error validating block")
    (validate-block* block)))

(defn validate-blocks [blocks]
  (if-not (sequential? blocks)
    (throw+ "Error validating blocks"))
  (doseq [block blocks]
    (try+
     (validate-block block)
     (catch Exception e
       (throw+))))
  true)

(defn find-essay-by-id [id]
  (let [essay (first (k/select db/essays
                               (where {:id id})))]
    (if essay (-> essay
                  (parse-document)) nil)))

(defn find-essay [& [params]]
  (let [query (cond-> (k/select* db/essays)
                (not (empty? params)) (where params))
        essays (k/select query)]
    (->> essays
         (map parse-document))))

(defn create-essay [params]
  (let [time (util/date-time)
        essay (merge
               params
               {:document nil
                :draft nil
                :state "draft"
                :created_at time
                :updated_at time
                :published_at nil})
        record (k/insert db/essays
                         (values essay))
        id (get-in record [(keyword "last_insert_rowid()")])]
    (merge essay {:id id})))

(defn update-essay [id {:keys [document draft] :as params}]
  (let [current (find-essay-by-id id)
        fields (merge params
                      (if document {:document (json/generate-string document)})
                      (if draft {:draft (json/generate-string draft)})
                      (if (= (:state current) "public") {:state "edited"}))]
    (k/update db/essays
              (set-fields fields)
              (where {:id id}))))

(defn delete-essay [id]
  (k/delete db/essays
            (where {:id id})))

(defn publish-essay [id]
  (let [essay (find-essay-by-id id)
        new-state (if (= (:state essay) "edited") "edited" "public")]
    (k/update db/essays
              (set-fields {:state new-state})
              (where {:id id}))
    (merge essay {:state new-state})))

;; FIXME: exists? returnning large map cause InternalError with zero stack track
