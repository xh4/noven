(ns noven.resources.media
  (:require [noven.util :as util]
            [noven.db.core :as db]
            [noven.storage :as storage]
            [noven.resources.conf :as conf]
            [noven.resources.file :refer :all]
            [noven.resources.site :as site]
            [korma.core :as k :refer [where values limit
                                      order set-fields]]
            [clojure.java.io :as io]
            [taoensso.timbre :as timbre]
            [me.raynes.fs :as fs]))

(timbre/refer-timbre)

(defn make-media-entity [{:keys [id] :as file}]
  (let [time (util/date-time)]
    {:file_id id
     :type "image"
     :created_at time
     :updated_at time}))

(defn create-media [{ring-file-map :file
                     :as params}]
  (let [file (storage/handle-upload ring-file-map)
        media-entity (make-media-entity file)
        return (k/insert db/media
                         (values media-entity))
        id (get-in return [(keyword "last_insert_rowid()")])]
    (when (:head params)
      (site/set-kv :last_import_id id))
    (merge media-entity {:id id
                         :file file})))

(defn build-query [params]
  (merge {:trash (if (:trash params) (:trash params) false)}
         (when (:collection_id params) {:collection_id (:collection_id params)})))

(defn associate-file [media-entity]
  (let [file (find-file-by-id (:file_id media-entity))]
    (merge media-entity {:file file})))

(defn find-media-by-id [id]
  (let [media-entity (first (k/select db/media
                                      (where {:id id})))
        file (find-file-by-id (:file_id media-entity))]
    (when (and media-entity file)
      (merge media-entity {:file file}))))

;; FIXME: empty WHERE predicates cause error
;;        https://github.com/korma/Korma/issues/80
;;        single WHERE map is ok, multiple maps still cause error

(defn find-media [params]
  (let [last-import-id (site/get-by-key "last_import_id")
        no-order (-> (k/select* db/media)
                     (where (build-query params))
                     (limit (or (:limit params) 99999)))
        query (apply order no-order (or (:order params) [:created_at :DESC]))
        media-entities (cond
                         (:last_import params) (k/select (where query {:id [>= last-import-id]}))
                         (:after params) (k/select (where query {:id [> (:after params)]}))
                         :else (k/select query))]
    (map associate-file media-entities)))

;; FIXME: set-fields with empty map cause sql syntax error

(defn update-media [id params]
  (k/update db/media
            (set-fields params)
            (where {:id id})))

;; FIXME: PUT with file parameter(1.7Mb) which expecting to be a string
;;        will cause 500 Internal server error

(defn trash-media-by-ids [ids]
  (k/update db/media
            (set-fields {:trash true})
            (where {:id [in ids]})))

(defn delete-media [id]
  (k/delete db/media
            (where {:id id})))
