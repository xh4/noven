(ns noven.resources.transformer
  (:require [cheshire.core :as json]
            [slingshot.slingshot :refer [try+ throw+]]))

(defn STRING [v]
  (if (string? v)
    v
    (throw+ "Not a string.")))

(defn INTEGER [v]
  (try+
   (Integer. (re-find #"\A-?\d+" v))
   (catch Object _
     (throw+ "Error parse Integer."))))

(defn BOOLEAN [v]
  (condp = v
    "true" true
    "1" true
    "false" false
    "0" false
    (throw+ "Error parse Boolean.")))

(defn JSON [v]
  (try+
   (json/parse-string v)
   (catch Object _
     (throw+ "Error parse JSON."))))

(defn FILE [v]
  (if (map? v)
    v
    (throw+ "Not a file.")))

(defn transform-params [params transformer]
  (reduce
   (fn [transformed pair]
     (let [key (first pair)
           value (second pair)
           transform-fn (get transformer key)]
       (if transform-fn
         (try+
          (let [transformed-value (transform-fn value)]
            (assoc-in transformed [:params key] transformed-value))
          (catch string? message
            (assoc-in transformed [:errors key] message)))
         (assoc-in transformed [:params key] value))))
   {:params {} :errors {}}
   params))
