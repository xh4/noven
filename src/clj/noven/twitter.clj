(ns noven.twitter
  (:require [cheshire.core :as json]
            [clojure.java.io :as io]
            [noven.db.core :as db]
            [noven.storage :as storage]
            [noven.util :refer [stack-trace]]
            [taoensso.timbre :as timbre]
            [me.raynes.fs :as fs]
            [clj-http.client :as client]
            [clj-http.conn-mgr :as conn-mgr]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clojure.string :as string])
  (:use [korma.core])
  (:import [com.twitter Autolink]
           [java.net URL]
           [org.joda.time DateTime]
           [org.joda.time.format ISODateTimeFormat]
           [org.apache.commons.io FilenameUtils]
           [twitter4j TwitterFactory MediaEntity MediaEntity$Size Paging]
           [twitter4j.conf ConfigurationBuilder]))

(timbre/refer-timbre)

(def ^:dynamic *consumer-key* "YOUR VALUE HERE")
(def ^:dynamic *consumer-secret* "YOUR VALUE HERE")
(def ^:dynamic *access-token* "YOUR VALUE HERE")
(def ^:dynamic *access-token-secret* "YOUR VALUE HERE")

(def twitter (let [cb (ConfigurationBuilder.)
                   _ (do
                       (.setDebugEnabled cb true)
                       (.setOAuthConsumerKey cb *consumer-key*)
                       (.setOAuthConsumerSecret cb *consumer-secret*)
                       (.setOAuthAccessToken cb *access-token*)
                       (.setOAuthAccessTokenSecret cb *access-token-secret*)
                       ;; (.setHttpProxyHost cb "localhost")
                       ;; (.setHttpProxyPort cb 8118)
                       )
                   tf (TwitterFactory. (.build cb))]
               (.getInstance tf)))

(def linker (Autolink.))

(defn make-media-size [size-entity version]
  (let [value (.getValue size-entity)
        resize (.getResize value)]
    {:width (.getWidth value)
     :height (.getHeight value)
     :version version
     :resize (cond
               (= resize MediaEntity$Size/CROP) :crop
               (= resize MediaEntity$Size/FIT) :fit)}))

(defn make-media-entity [e]
  (if e
    {:id (.getId e)
     :url (.getMediaURL e)
     :basename (FilenameUtils/getBaseName (.getMediaURL e))
     :extension (FilenameUtils/getExtension (.getMediaURL e))
     :sizes (map make-media-size (.getSizes e) [:thumb :small :medium :large])}))

(defn make-tweet [t]
  {:id (.getId t)
   :text (.getText t)
   :cooked (.autoLink linker (.getText t))
   :media (make-media-entity (first (.getMediaEntities t)))
   :time (DateTime. (.getCreatedAt t))})

(defn get-user-timeline []
  (let [paging (Paging. 1 (Integer. 100))]
    (map make-tweet (.getUserTimeline twitter paging))))

(defn fetch-image [tweet url]
  ;; http://pbs.twimg.com/tweet_video_thumb/B0fFHI4CQAEG1ov.png:small
  ;; http://pbs.twimg.com/media/B06VQa_IcAEETxO.jpg:thumb
  (try
    (let [basename (FilenameUtils/getBaseName url)
          ext (first (string/split (FilenameUtils/getExtension url) #":"))
          [_ version] (re-find #":(\w+)$" url)]
      (let [file (io/file (str "resources/Cache/" basename "-" version "." ext))]
        (when-not (.exists file)
          (info (str "fetching image " url))
          (let [res (client/get url
                                {:as :byte-array
                                 :conn-timeout 10000
                                 :socket-timeout 60000
                                 ;; :proxy-host "127.0.0.1"
                                 ;; :proxy-port 8118
                                 :throw-exceptions true})
                bytes (:body res)]
            (with-open [out (clojure.java.io/output-stream file)]
              (.write out bytes))
            (storage/add-file {:file file
                               :filname (str basename "." ext)
                               :version version
                               :tag (str "tweet:" (:id tweet))})))))
    (catch Exception e
      (debug (str "fail to fetch image " url))
      (debug (stack-trace e)))))

(defn fetch-media [tweet]
  (when (:media tweet)
    (info (str "fetching media for: " (:id tweet)))
    (doall
     (map (fn [size]
            (let [url (str (get-in tweet [:media :url])
                           (:version size))]
              (fetch-image tweet url)))
          (get-in tweet [:media :sizes])))))

(defn serialize-tweet [t]
  (-> t
      (assoc :time (f/unparse (ISODateTimeFormat/basicDateTime) (:time t)))
      (json/generate-string)))

(defn make-record [t]
  (let [content (serialize-tweet t)
        tid (:id t)]
    {:tid tid
     :content content}))

(defn fetch-tweets []
  (info "fetching tweets...")
  (try
    (let [tweets (get-user-timeline)]
      (doall (map fetch-media tweets))
      (delete db/tweets
              (where {:id [>= 0]}))
      (insert db/tweets
              (values (map make-record tweets)))
      (info "tweets fetched: " (count tweets)))
    (catch Exception e
      (debug (str "failed fetching tweets: " (.getMessage e)))
      (debug (stack-trace e)))))

;; nil or future
(defonce job (atom nil))

(defn start-job []
  (when-not @job
    (info "start job")
    (reset! job (future
                  (loop []
                    (fetch-tweets)
                    (Thread/sleep (* 1000 30))
                    (if-not (Thread/interrupted)
                      (recur)))))))

(defn stop-job []
  (when @job
    (info "stop job")
    (while (and @job
                (not (future-cancelled? @job)))
      (future-cancel @job)
      (reset! job nil))))
