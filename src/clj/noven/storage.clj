(ns noven.storage
  (:require [noven.resources.file :refer :all]
            [noven.env :as env]
            [noven.util :as util]
            [cheshire.core :as json]
            [clojure.string :as string]
            [clojure.java.io :as io]
            [me.raynes.fs :as fs]
            [compojure.core :refer [defroutes GET]])
  (:import [java.awt.image BufferedImageOp]
           [javax.imageio ImageIO]
           [javaxt.io Image]
           [java.io File]))

(defn file-dir [file-record]
  (let [hash (:hash file-record)
        d1 (subs hash 0 2)
        d2 (subs hash 2 4)]
    (str env/*path* "/Storage/" d1 "/" d2)))

(defn file-path [file-record]
  (str (file-dir file-record) "/" (:name file-record)))

(defn get-file [file-record]
  (io/file (file-path file-record)))

(defn add-file [{:keys [filename content-type size parent version tag] :as fmap}]
  (let [source-file (:file fmap)
        filename (or filename (.getName source-file))
        content-type (or content-type "image/jpg")
        size (or size (.length source-file))
        version (or version "original")
        timestamp (util/timestamp)
        hash (util/md5 (str filename timestamp))
        buffer (ImageIO/read source-file)
        metadata {:width (.getWidth buffer)
                  :height (.getHeight buffer)}
        record {:name filename
                :size size
                :type content-type
                :parent_id parent
                :version version
                :tag tag
                :hash hash
                :timestamp timestamp
                :metadata metadata}
        dir (file-dir record)
        path (file-path record)]
    (fs/mkdirs dir)
    (io/copy source-file (io/as-file path))
    (create-file record)))

(def max-sides {:large 1024
                :medium 500
                :small 240
                :thumbnail 100})

(defn resize-image [file max-size]
  (let [image (Image. file)
        output-file (File/createTempFile (util/uuid) ".jpg")]
    (.resize image max-size max-size true)
    (.saveAs image output-file)
    output-file))

(defn make-version [id version]
  (when-let [file-record (find-file-by-id id)]
    (when-let [original-file (get-file file-record)]
      (when-let [max-side ((keyword version) max-sides)]
        (let [versioned-file (resize-image original-file max-side)]
          (add-file {:file versioned-file
                     :filename (:name file-record)
                     :parent id
                     :version version
                     :content-type (:type file-record)})
          versioned-file)))))

(defn serve-file [id filename version]
  (let [version (or version "original")]
    (if-let [file-record (find-file-versions-by-id id version)]
      (when (= (:name file-record) filename)
        (get-file file-record))
      (make-version id version))))

(defn handle-upload [{:keys [tempfile
                             filename
                             content-type
                             size] :as ring-file-map}]
  (add-file {:file tempfile
             :filename filename
             :content-type content-type
             :size size}))

(defroutes storage-routes
  (GET "/storage/:id/:filename" [id filename version] (serve-file id filename version)))
