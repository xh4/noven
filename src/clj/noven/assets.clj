(ns noven.assets
  (:require [clojure.java.io :as io]
            [me.raynes.fs :as fs]
            [compojure.core :refer [defroutes GET]]
            [compojure.route :refer [resources]])
  (:import [java.io File]
           [java.util.regex Pattern]))

(def fsep (java.io.File/separator))

(def fsep-regex (java.util.regex.Pattern/quote fsep))

(defn- normalize-path [^String path]
  (if (= fsep "/")
    path
    (.replaceAll path fsep-regex "/")))

(defn- get-path
  "Given asset file and assets root, return relative path of this asset file"
  [file base-dir]
  (->> file
       (.toURI)
       (.relativize (.toURI base-dir))
       (.getPath)
       (normalize-path)))

(def filter-regex #".*\.(css|js|jpg|jpeg|png|gif|ico|svg)$")

(def remove-regexes [#".*node_modules/.*"])

(defn slurp-assets
  "Given assets directory, return a file sequence of asset files"
  [dir]
  (let [dir (io/as-file dir)]
    (->> (file-seq dir)
         (remove (fn [file]
                   (let [path (get-path file dir)]
                     (some (fn [regex]
                             (re-find regex path)) remove-regexes))))
         (filter #(re-find filter-regex (get-path % dir)))
         (reduce #(conj %1 [(get-path %2 dir) %2]) {}))))

(defn asset-path
  "Given a simplified asset path, return the complete one"
  [path]
  (let [ext (subs (fs/extension path) 1)
        base-dir (cond
                   (= ext "js") "javascripts"
                   (= ext "css") "stylesheets"
                   (contains? #{"jpg" "gif" "png" "jpeg" "ico" "svg"} ext) "images")]
    (if base-dir
      (str base-dir "/" path)
      path)))

(defn serve-file
  "Given a simplified path of asset file, return the file or nil if the file not in assets path.
  The path is like `vendor/normalize.css`"
  [path]
  (if (re-find filter-regex path)
    (or (io/resource (str "april_1st/assets/" (asset-path path)))
        (io/resource (str "assets/" (asset-path path))))))

(defroutes assets-routes
  (GET "/assets/*" {{path :*} :params} (serve-file path))
  (resources "/assets/vendor" {:root "react"}))
