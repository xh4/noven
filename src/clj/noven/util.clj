(ns noven.util
  (:require [pandect.algo.md5 :as md5]
            [clojure.java.io :as io]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clj-time.coerce :as c]
            [clj-time.local :as l]
            [me.raynes.fs :as fs]
            [clojure.string :as string])
  (:import java.util.UUID
           [java.io StringWriter PrintWriter File FileInputStream]
           [java.net URLDecoder URLEncoder]))

(defn uuid [] (str (java.util.UUID/randomUUID)))

(defn md5 [s] (md5/md5 s))

(defn now [] (l/local-now))

(defn timestamp [] (int (/ (c/to-long (now)) 1000)))

(defn date-time [] (l/format-local-time (l/local-now) :date-time))

(defn parse-int [s]
  (try
    (Integer. (re-find #"\d+" s))
    (catch Exception e
      nil)))

(defn random-time []
  (let [random-month #(let [month (range 1 13)]
                        (nth month (rand-int (count month))))
        random-year #(let [year [2012 2013 2014 2015]]
                       (nth year (rand-int (count year))))
        rt (t/date-time (random-year) (random-month) 1 23 0 0)]
    (date-time rt)))

(defn stack-trace [e]
  (let [sw (StringWriter.)]
    (.printStackTrace e (PrintWriter. sw))
    (.toString sw)))

(defn resolve-path [path]
  (-> path
      (fs/expand-home)
      (io/file)
      (.getCanonicalPath)))

(defmulti open-resource
  "Returns input stream about the resource specified by url, or nil if an
  appropriate resource does not exist.

  This dispatches on the protocol of the URL as a keyword, and
  implementations are provided for :file and :jar. If you are on a
  platform where (Class/getResource) returns URLs with a different
  protocol, you will need to provide an implementation for that
  protocol."
  {:arglists '([url])}
  (fn [^java.net.URL url]
    (keyword (.getProtocol url))))

(defn- ^File url-as-file [^java.net.URL u]
  (-> (.getFile u)
      (string/replace \/ File/separatorChar)
      (string/replace "+" (URLEncoder/encode "+" "UTF-8"))
      (URLDecoder/decode "UTF-8")
      io/as-file))

(defmethod open-resource :file
  [url]
  (if-let [file (url-as-file url)]
    (if-not (.isDirectory file)
      (FileInputStream. file))))

(defn- add-ending-slash [^String path]
  (if (.endsWith path "/")
    path
    (str path "/")))

(defn- jar-directory? [^java.net.JarURLConnection conn]
  (let [jar-file   (.getJarFile conn)
        entry-name (.getEntryName conn)
        dir-entry  (.getEntry jar-file (add-ending-slash entry-name))]
    (and dir-entry (.isDirectory dir-entry))))

(defmethod open-resource :jar
  [^java.net.URL url]
  (let [conn (.openConnection url)]
    (if-not (jar-directory? conn)
      (.getInputStream conn))))

(defn resource
  "Returns a packaged resource, or nil if the
  resource does not exist.
  Options:
  :root - take the resource relative to this root
  :loader - resolve the resource in this class loader"
  [path & [{:keys [root loader] :as opts}]]
  (let [path (-> (str (or root "") "/" path)
                 (.replace "//" "/")
                 (.replaceAll "^/" ""))]
    (if-let [resource (if loader
                        (io/resource path loader)
                        (io/resource path))]
      (open-resource resource))))
