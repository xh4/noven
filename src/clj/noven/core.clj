(ns noven.core
  (:require [noven.server :as server]
            [noven.env :as env]
            [noven.util :as util]
            [noven.db.core :as db]
            [noven.migration :as migration]
            [noven.migrations.m20150617104311]
            [noven.migrations.m20150701224804]
            [noven.migrations.m20150905115840]
            [noven.migrations.m20150916095637]
            [noven.migrations.m20150917125004]
            [clojure.string :as string]
            [clojure.java.io :as io]
            [clojure.tools.cli :refer [parse-opts]]
            [taoensso.timbre :as timbre]
            [me.raynes.fs :as fs]))

(timbre/refer-timbre)

(def cli-options
  [["-p" "--port PORT" "Port number"
    :default 4000
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ["-h" "--help"]])

(defn usage [options-summary]
  (->> ["Usage: [options] command [command-options]"
        ""
        "Commands:"
        "  server    Start a server"
        ""
        "Server Options:"
        options-summary]
       (string/join \newline)))

(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (string/join \newline errors)))

(defn exit [status msg]
  (println msg)
  (System/exit status))

(defn ensure-site-dir [path]
  (let [path (util/resolve-path path)
        dirs [["Storage"]
              ["Cache"]
              ["Logs"]]
        files [["database.db"]
               ["Logs" "Site.log"]]]

    (when-not (fs/directory? path)
      (println "Site directory not exists, creating new one..." )
      (if-not (fs/mkdir path)
        (exit 1 (str "Permission denied while making site directory: " path))
        (println "Making site directory: " path)))

    (if-not (fs/writeable? path)
      (exit 1 (str "Permission denied while accessing site directory: " path))
      (println "Using site directory: " path))

    (doseq [d dirs]
      (let [dir (apply io/file path d)]
        (when-not (.exists dir)
          (println "Making directory: " (.getPath dir))
          (fs/mkdirs dir))))

    (doseq [f files]
      (let [file (apply io/file path f)]
        (when-not (.exists file)
          (println "Creating file: " (.getPath file))
          (fs/touch file))))

    (env/-set-path path)))

(defn start-server [port & [path]]
  (ensure-site-dir path)

  (timbre/set-config! [:shared-appender-config :spit-filename] (str env/*path* "/Logs/Site.log"))

  (db/initialize)

  (migration/run-migrations)

  (try
    (println "Starting web server on port" port)
    (server/start port)
    (catch Exception e
      (error (util/stack-trace e))
      (exit 1 (str "Error while starting server on port " port ", see log file for more details.")))))

(defn main [& args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)]
    (cond
      (:help options) (exit 0 (usage summary))
      errors (exit 1 (error-msg errors)))
    (case (first arguments)
      "server" (start-server (:port options) (second arguments))
      (exit 1 (usage summary)))))
