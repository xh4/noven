(ns noven.env
  (:require [noven.util :as util]
            [environ.core :refer [env]]
            [me.raynes.fs :as fs]))

(defn development? []
  (= (env :environment) "development"))

(defn test? []
  (= (env :environment) "test"))

(defn production? []
  (= (env :environment) "production"))

(def ^:dynamic *path* (.getPath fs/*cwd*))

(defn -set-path [path]
  (def ^:dynamic *path* (util/resolve-path path)))

(defonce store (atom {}))

(defn set-env [k v]
  (swap! store assoc k v))

(defn get-env [k]
  (get @store k))
