(ns noven.api
  (:require
   ;; [noven.resources.library :refer [library]]
   [noven.routes.media :refer [media-entity
                               media-entities
                               media-publish
                               media-unpublish]]
   [noven.routes.essay :refer [essay
                               essays
                               essay-publish
                               essay-unpublish]]
   [noven.routes.content :refer [content
                                 contents]]
   ;; [noven.routes.category :refer [category
   ;;                                categories
   ;;                                content-categories]]
   ;; [noven.routes.tag :refer [tag
   ;;                           tags]]
   [compojure.core :refer :all]))

(defroutes api-routes
  (context "/api" []
    ;; (ANY "/library" [] (library))
    ;; (ANY "/library/*" [] (library))

    (ANY "/media" [] (media-entities))
    (ANY "/media/:id" [id] (media-entity id))
    (ANY "/media/:id/publish" [id] (media-publish id))
    (ANY "/media/:id/unpublish" [id] (media-unpublish id))

    (ANY "/essays" [] (essays))
    (ANY "/essays/:id" [id] (essay id))
    (ANY "/essays/:id/publish" [id] (essay-publish id))
    (ANY "/essays/:id/unpublish" [id] (essay-unpublish id))

    (ANY "/contents" [] (contents))
    (ANY "/contents/:id" [id] (content id))

    ;; (ANY "/categories/:id" [id] (category id))
    ;; (ANY "/categories" [] (categories))
    ;; (ANY "/contents/:content-id/categories" [content-id]
    ;;   (content-categories content-id))

    ;; (ANY "/tags/:id" [id] (tag id))
    ;; (ANY "/tags" [] (tags))
    ;; (ANY "/contents/:content-id/tags" [content-id]
    ;;   (content-tags content-id))
    ))
