(ns noven.fixtures
  (:require [clojure.java.io :as io]))

(def test-image (io/file (io/resource "fixtures/test.jpg")))
