(ns noven.dashboard.macros
  (:require [om-tools.core :refer [defcomponent]]
            [om-tools.dom :refer [div span label]]))

(defmacro scroll [& inner]
  (let [sym# (gensym)]
    `(do
       (defcomponent ~sym# [~(symbol "_") owner#]
         (~(symbol "render") [~(symbol "_")]
          (div {:class "scroll_wrapper"}
            (div {:class "scroll_track"
                  :style {:opacity 1}}
              (div {:class "scroll_track_footer"}
                (div {:class "scroll_bar"}
                  (div {:class "scroll_bar_grab"}))))
            (div {:class "scroll_content"}
              ~@inner)))
         (~(symbol "did-mount") [~(symbol "_")]
          (let [wrapper# (om/get-node owner#)]
            (noven.dashboard.util/sync-scroll wrapper#)
            (events/removeAll wrapper#)
            (goog.events/listen wrapper# "scroll"
              #(noven.dashboard.util/sync-scroll wrapper#))
            (goog.events/listen js/window "resize"
              #(noven.dashboard.util/sync-scroll wrapper#)))))
       (om/build ~sym# {}))))

(defmacro col-row [name & contents]
  `(div {:class "col-row"}
     (div {:class "label"}
       (label ~name))
     (div {:class "value"} ~@contents)))
