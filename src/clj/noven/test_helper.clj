(ns noven.test-helper
  (:require [noven.env :as env]
            [noven.core :as core]
            [noven.db.core :as db]
            [ring.mock.request :as mock]
            [noven.server :refer [handler]]
            [noven.migration :as migration]
            [noven.migrations.m20150617104311]
            [noven.migrations.m20150701224804]
            [noven.migrations.m20150905115840]
            [noven.migrations.m20150916095637]
            [noven.migrations.m20150917125004]
            [noven.assertions :as assertions]
            [clojure.test :refer :all]
            [cheshire.core :as json]
            [me.raynes.fs :as fs]))

(defn setup-site [f]
  ;; create site
  (let [tmp-path (.. (fs/temp-dir "")
                     (toPath)
                     (toString))]
    (core/ensure-site-dir tmp-path)
    (env/set-env "username" "foo")
    (env/set-env "password" "bar")
    (db/initialize)
    (migration/run-migrations))
  (f)
  ;; clean site
  (let [current-path env/*path*]
    (fs/delete-dir current-path)))

(defn response-type [response]
  (let [content-type (get (:headers response) "Content-Type")]
    (condp #(.startsWith %2 %1) content-type
      "application/json" :json
      "text/html" :html
      nil)))

(defn response-body [response]
  (let [body (:body response)]
    (condp = (response-type response)
      :json (json/parse-string body true)
      :html body
      nil)))

(defmacro with-response [response-expr & body]
  `(let [~(symbol "response") ~response-expr
         ~(symbol "assert-status")
         (fn [status#]
           (assertions/assert-response-status ~(symbol "response") status#))

         ~(symbol "assert-redirected-to")
         (fn [path#]
           (assertions/assert-redirected-to ~(symbol "response") path#))]
     ~@(map
        (fn [expr]
          (if (list? expr)
            (if (= (first expr) (symbol "with-body"))
              `(let [~(second expr) (response-body ~(symbol "response"))]
                 ~@(drop 2 expr))
              expr)
            expr))
        body)))

(defn request [method uri & [params]]
  (handler (mock/request method uri params)))
