(ns noven.resource
  (:require [noven.util :as util]
            [noven.db.core :as db]
            [noven.api :refer [api-routes]]
            [ring.mock.request :as mock]
            [cheshire.core :as json]
            [clojure.string :as string]
            [korma.core :as k :refer [where values limit
                                      order set-fields]]))

(defn get-resource [name id])

(defn find-resource [name params])

(defn create-resource [name params])

(defn update-resource [name id])

(defn delete-resource [name id])

(defn api-resource [path & [params]]
  (let [response (api-routes (mock/request :get path params))
        content-type (get (:headers response) "Content-Type")
        body (:body response)]
    (when (.startsWith content-type "application/json")
      (json/parse-string body true))))
