(ns noven.migrations.m20150917125004
  (:require [noven.migration :as migration])
  (:use [clojure.java.jdbc]))

(defn migrate []
  (execute! (migration/get-db) ["DROP TABLE text_posts;"]))
