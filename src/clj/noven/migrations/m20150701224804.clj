(ns noven.migrations.m20150701224804
  (:require [noven.migration :as migration]))

(def tables
  [{:name "media"
    :columns [[:id "integer" "primary key"]
              [:file_id "integer" "not null"]
              [:type "text" "not null"]
              [:title "text"]
              [:caption "text"]
              [:license "text"]
              [:download "text"]
              [:trash "boolean" "not null" "default 0"]
              [:created_at "datetime" "not null"]
              [:updated_at "datetime" "not null"]
              [:published_at "datetime"]]}
   {:name "categories"
    :columns [[:id "integer" "primary key"]
              [:parent_id "integer"]
              [:name "text" "not null"]
              [:sort "integer"]]}
   {:name "content_categories"
    :columns [[:id "integer" "primary key"]
              [:content_id "integer" "not null"]
              [:category_id "integer" "not null"]]}
   {:name "tags"
    :columns [[:id "integer" "primary key"]
              [:name "text" "not null"]
              [:sort "integer"]]}
   {:name "content_tags"
    :columns [[:id "integer" "primary key"]
              [:content_id "integer" "not null"]
              [:tag_id "integer" "not null"]]}
   {:name "contents"
    :columns [[:id "integer" "primary key"]
              [:type "text" "not null"]
              [:detail_id "integer" "not null"]
              [:slug "text"]
              [:created_at "datetime" "not null"]
              [:updated_at "datetime" "not null"]]}
   {:name "site"
    :columns [[:id "integer" "primary key"]
              [:key "text" "not null"]
              [:value "text"]]}])

(defn migrate []
  (migration/create-tables tables))
