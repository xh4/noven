(ns noven.migrations.m20150617104311
  (:require [noven.migration :as migration]))

(def tables
  [{:name "files"
    :columns [[:id "integer" "primary key"]
              [:parent_id "integer"]
              [:name "text" "not null"]
              [:type "text" "not null"]
              [:size "integer" "not null"]
              [:version "text" "not null"]
              [:metadata "text"]
              [:hash "text" "not null"]
              [:tag "text"]
              [:timestamp "integer" "not null"]]}
   {:name "tweets"
    :columns [[:id "integer" "primary key"]
              [:tid "integer" "not null"]
              [:content "text" "not null"]]}
   {:name "text_posts"
    :columns [[:id "integer" "primary key"]
              [:content "text" "not null"]]}])

(defn migrate []
  (migration/create-tables tables))
