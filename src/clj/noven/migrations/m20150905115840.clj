(ns noven.migrations.m20150905115840
  (:require [noven.migration :as migration]))

(def tables
  [{:name "essays"
    :columns [[:id "integer" "primary key"]
              [:title "text" "not null"]
              [:excerpt "text"]
              [:document "text"]
              [:draft "text"]
              [:state "text" "not null"]
              [:created_at "datetime" "not null"]
              [:updated_at "datetime" "not null"]
              [:published_at "datetime"]]}])

(defn migrate []
  (migration/create-tables tables))
