(ns noven.migrations.m20150916095637
  (:require [noven.migration :as migration])
  (:use [clojure.java.jdbc]))

(def tables
  [{:name "essays"
    :columns [[:id "integer" "primary key"]
              [:title "text"]
              [:excerpt "text"]
              [:document "text"]
              [:draft "text"]
              [:state "text" "not null"]
              [:created_at "datetime" "not null"]
              [:updated_at "datetime" "not null"]
              [:published_at "datetime"]]}])

(defn migrate []
  (execute! (migration/get-db) ["ALTER TABLE essays RENAME TO tmp_essays;"])
  (migration/create-tables tables)
  (execute! (migration/get-db) ["INSERT INTO essays (id, title, excerpt, document, draft, state, created_at, updated_at, published_at) SELECT id, title, excerpt, document, draft, state, created_at, updated_at, published_at FROM tmp_essays;"])
  (execute! (migration/get-db) ["DROP TABLE tmp_essays;"]))
