(ns noven.dashboard.collections
  (:require-macros [noven.dashboard.macros :refer [scroll]])
  (:require [noven.dashboard.appstate :refer [app-state]]
            [noven.dashboard.util :as util]
            [noven.dashboard.media :refer [media-viewer]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span ul li a img]]
            [goog.dom :as dom]
            [goog.style :as style]
            [goog.events :as events]
            [goog.string :as gstring]
            [goog.dom.dataset :as dataset]
            [goog.dom.classes :as classes]
            [goog.string.format]
            [clojure.string :as string]
            [ajax.core :refer [GET POST PUT DELETE]]))

(defn read-location [tab]
  (let [[tab-name location] (string/split (name tab) "#")]
    (cond
      (nil? location) nil
      (contains? #{"all-pictures" "last-import" "trash"} location) location
      :else (js/parseInt location))))

(defcomponent collection
  [{:keys [collection-name media-count location cover]} owner]
  (render [_]
    (a {:class "gallery"
        :href (str "#library/collections/" location)}
       (when cover
         (let [file (:file cover)
               {:keys [width height]} (:metadata file)
               zoom 5
               box-size (+ 100 (* 20 zoom))
               {left :left
                top :top
                display-width :width
                display-height :height} (util/cover box-size
                                                    box-size
                                                    width
                                                    height)]
           (div {:class "cover"}
             (img {:width display-width
                   :height display-height
                   :src (util/make-url file "s")
                   :style {:display "block"
                           :position "absolute"
                           :left (str left "px")
                           :top (str top "px")}}))))
       (div {:class "overlay"})
       (div {:class "metadata"}
         (div {:class "title"}
           collection-name)
         (div {:class "pictures-count"}
           (condp = media-count
             nil ""
             0 ""
             1 "1 item"
             (str media-count " items")))))))

(defcomponent collections
  [{:keys [library media] :as props} owner]
  (init-state [_]
    {:all-pictures {:cover nil :media-count nil}
     :last-import {:cover nil :media-count nil}
     :trash {:cover nil :media-count nil}})
  (render [_]
    (scroll
     (div {:id "collections"
           :style {:right (if (get-in library [:inspector :display]) "280px" 0)}}
       (div {:class "galleries"}
         (om/build collection {:collection-name "All pictures"
                               :media-count (om/get-state owner [:all-pictures :media-count])
                               :location "all-pictures"
                               :cover (om/get-state owner [:all-pictures :cover])})
         (om/build collection {:collection-name "Last import"
                               :media-count (om/get-state owner [:last-import :media-count])
                               :location "last-import"
                               :cover (om/get-state owner [:last-import :cover])})
         (om/build collection {:collection-name "Trash"
                               :media-count (om/get-state owner [:trash :media-count])
                               :location "trash"
                               :cover (om/get-state owner [:trash :cover])}))
       (div {:class "section-header"}
         (div {:class "separator"})
         (div {:class "title"}
           "My Collections"))
       (div {:style {:font-size "13px"
                     :padding "12px"}}
         "Under development..."))))
  (did-mount [_]
    (doseq [col [:all-pictures
                 :last-import
                 :trash]]
      (let [url (condp = col
                  :all-pictures "/api/media"
                  :last-import "/api/media?last_import=true"
                  :trash "/api/media?trash=true")]
        (GET url
            {:response-format :json
             :keywords? true
             :handler (fn [res]
                        (let [m (first res)]
                          (om/set-state! owner col {:cover m
                                                    :media-count (count res)})))})))))

(defn open-collection
  [location {:keys [library media] :as props} owner]
  (om/set-state! owner :location location)
  (om/update! media [])
  (let [url (condp = location
              "all-pictures" "/api/media"
              "last-import" "/api/media?last_import=true"
              "trash" "/api/media?trash=true")]
    (GET url
        {:response-format :json
         :keywords? true
         :handler (fn [m]
                    (om/update! media (vec m)))})))

(defcomponent library-collections
  [{:keys [library media] :as props} owner]
  (init-state [_]
    {:location nil})
  (render [_]
    (let [location (read-location (:tab library))]
      (if (nil? location)
        (om/build collections (select-keys props [:library
                                                  :media]))
        (om/build media-viewer (select-keys props [:library
                                                   :media])))))
  (did-mount [_]
    (om/update! media [])
    (let [location (read-location (:tab library))]
      (when location
        (open-collection location props owner))))
  (will-update [_ {:keys [library media]} _]
    (let [location (read-location (:tab library))]
      (when-not location
        (om/update! media []))
      (when (and location
                 (not (= location (om/get-state owner :location))))
        (open-collection location props owner)))))
