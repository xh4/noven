(ns noven.dashboard.util
  (:require [om.core :as om :include-macros true]
            [goog.style :as style]
            [goog.dom :as dom]
            [goog.string]
            [goog.string.format]
            [goog.events :as events]
            [goog.dom.classes :as classes]
            [clojure.string :as string]))

(defn indexed
  "Returns a lazy sequence of [index, item] pairs, where items come
  from 's' and indexes count up from zero.

  (indexed '(a b c d))  =>  ([0 a] [1 b] [2 c] [3 d])"
  [s]
  (map vector (iterate inc 0) s))

(defn positions
  "Returns a lazy sequence containing the positions at which pred
  is true for items in coll."
  [pred coll]
  (for [[idx elt] (indexed coll) :when (pred elt)] idx))

(defn id-index [coll id]
  (first (positions #(= (:id %) id) coll)))

(defn transact!
  ([cursor id f]
   (transact! cursor id [] f))
  ([cursor id korks f]
   (let [idx (id-index @cursor id)
         korks (apply vector idx korks)]
     (om/transact! cursor korks f))))

(defn update!
  ([cursor id v]
   (transact! cursor id [] (fn [_] v)))
  ([cursor id korks v]
   (transact! cursor id korks (fn [_] v))))

(defn fetch [coll id]
  (if (om/cursor? coll)
    (fetch @coll id)
    (first (filter #(= (:id %) id) coll))))

(defn delete! [coll id]
  (if (om/cursor? coll)
    (om/update! coll (delete! @coll id))
    (vec (remove #(= (:id %) id) coll))))

(defn index-of [coll v]
  (let [i (count (take-while #(not= v %) coll))]
    (when (or (< i (count coll))
              (= v (last coll)))
      i)))

(defn vec-contains? [vec v]
  (if (index-of vec v) true false))

(defn find-index [coll f]
  (let [i (count (take-while #(not (f %)) coll))]
    (when (or (< i (count coll))
              (f (last coll)))
      i)))

(defn format [fmt & args]
  (apply goog.string/format fmt args))

(defn interface-width []
  (let [document-width (.-width (style/getSize (.-body js/document)))]
    (if (< document-width 950) 950 document-width)))

(defn interface-height []
  (let [container-height (.-height (style/getSize (dom/$ "container")))]
    (- container-height 45)))

(defn make-url [file version]
  (let [id (:id file)
        filename (:name file)]
    (str "/storage/" id "/" filename "?version=" version)))

(defn read-tab [tab]
  (when tab
    (let [[tab-name location] (string/split (name tab) "#")]
      [tab-name (cond
                  (nil? location) nil
                  (re-find #"^\d+$" location) (js/parseInt location)
                  :else location)])))

(defn fit-size
  [box-width box-height object-width object-height]
  (let [radio-x (if (<= object-width box-width)
                  1
                  (/ box-width object-width))
        radio-y (if (<= object-height box-height)
                  1
                  (/ box-height object-height))
        radio (min radio-x radio-y)
        display-width (* radio object-width)
        display-height (* radio object-height)
        left (/ (- box-width display-width) 2)
        top (/ (- box-height display-height) 2)]
    {:left left
     :top top
     :width display-width
     :height display-height}))

(defn cover
  [box-width box-height object-width object-height]
  (if (or (< object-width box-width)
          (< object-height box-height))
    (fit-size box-width box-height object-width object-height)
    (let [radio-x (if (<= object-width box-width)
                    1
                    (/ box-width object-width))
          radio-y (if (<= object-height box-height)
                    1
                    (/ box-height object-height))
          radio (max radio-x radio-y)
          display-width (* radio object-width)
          display-height (* radio object-height)
          left (/ (- box-width display-width) 2)
          top (/ (- box-height display-height) 2)]
      {:left left
       :top top
       :width display-width
       :height display-height})))

(defn sync-scroll [wrapper]
  (let [scroll-content (dom/getElementByClass "scroll_content" wrapper)
        scroll-track (dom/getElementByClass "scroll_track" wrapper)
        scroll-bar (dom/getElementByClass "scroll_bar" wrapper)
        track-footer (dom/getElementByClass "scroll_track_footer" wrapper)
        content-height (.-height (style/getSize scroll-content))
        wrapper-height (.-height (style/getSize wrapper))
        d (/ wrapper-height content-height)
        grab-height (* d wrapper-height)]
    (if (<= content-height wrapper-height)
      (do
        (style/setStyle scroll-bar "display" "none")
        (classes/add scroll-track "scroll_track_off"))
      (do
        (style/setStyle scroll-bar "display" "block")
        (classes/remove scroll-track "scroll_track_off")
        (style/setHeight scroll-bar (str (* d 100) "%"))
        (let [scroll-top (.-scrollTop wrapper)
              d (/ scroll-top content-height)]
          (style/setStyle scroll-bar "top" (str (* d 100) "%")))))))
