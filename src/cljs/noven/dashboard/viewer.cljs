(ns noven.dashboard.viewer
  (:require [noven.dashboard.util :as util]
            [noven.dashboard.appstate :refer [app-state]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span li a img]]
            [goog.dom :as dom]
            [goog.style :as style]
            [goog.events :as events]
            [goog.string :as gstring]
            [goog.dom.dataset :as dataset]
            [goog.dom.classes :as classes]
            [goog.string.format]
            [clojure.string :as string]
            [ajax.core :refer [GET POST PUT DELETE]]))

(defn current-id [tab]
  (let [[tab-name location] (string/split (name tab) "#")]
    (js/parseInt location)))

(defn viewport-size [viewport-display?]
  {:width (if viewport-display?
            (- (util/interface-width) 280)
            (util/interface-width))
   :height (util/interface-height)})

(defcomponent control-buttons
  [{:keys [library media] :as props} owner]
  (render-state [_ state]
    (let [current (current-id (:tab library))
          index (util/index-of (map #(:id %) media) current)
          first? (= index 0)
          last? (= index (- (count media) 1))]
      (when-not (empty? media)
        (div {}
          (when-not first?
            (a {:class "previous-button"
                :href (str "#library/media/" (:id (nth media (dec index))))}))
          (when-not last?
            (a {:class "next-button"
                :href (str "#library/media/" (:id (nth media (inc index))))})))))))

(defn load-image
  [{:keys [library media] :as props} owner]
  (let [current (current-id (:tab library))
        media-entity (util/fetch media current)
        file (:file media-entity)
        img (dom/createDom "img")]
    (when-not media-entity
      (GET (str "/api/media/" current)
          {:response-format :json
           :keywords? true
           :handler (fn [m]
                      (om/update! media [m]))}))
    (when (and media-entity
               (not= current (:id (om/get-state owner :media-entity))))
      (om/set-state! owner :loading? true)
      (om/set-state! owner :media-entity media-entity)
      (events/listenOnce img "load"
        (fn [_]
          (om/set-state! owner :loading? nil)))
      (aset img "src" (util/make-url file "large")))))

(defcomponent image-viewer
  [{:keys [library media] :as props} owner]
  (init-state [_]
    {:viewport (viewport-size (get-in library [:inspector :display]))
     :loading? true
     :media-entity nil})
  (render-state [_ {:keys [viewport] :as state}]
    (div {:id "image-viewer"
          :data-tab (:tab library)}
      (if (:loading? state)
        (div {:style {:padding "12px"}}
          "Loading...")
        (let [media-entity (:media-entity state)
              file (:file media-entity)
              metadata (:metadata file)
              {viewport-width :width
               viewport-height :height} viewport
              {left :left
               top :top
               display-width :width
               display-height :height} (util/fit-size viewport-width
                                                      viewport-height
                                                      (:width metadata)
                                                      (:height metadata))]
          (img {:src (util/make-url file "large")
                :style {:height (str display-height "px")
                        :width (str display-width "px")
                        :left (str left "px")
                        :top (str top "px")
                        :position "absolute"}})))

      (om/build control-buttons props)))
  (did-update [_ _ _]
    (load-image props owner))
  (did-mount [_]
    (load-image props owner)
    (events/listen js/window "resize"
      (fn [_]
        (om/set-state! owner :viewport
                       (viewport-size (get-in library [:inspector :display])))))))
