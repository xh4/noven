(ns noven.dashboard.dropline
  (:require [noven.dashboard.appstate :refer [app-state]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span ul ol li a img]]
            [cljs.core.async :refer [put! take! chan <!]]
            [goog.dom :as dom]
            [goog.events :as events]))

(defcomponent dropline [dropline owner]
  (render-state [_ {:keys [channel]}]
    (div {:class "dropline"
          :style {:display (if (:display dropline) "block" "none")
                  :top (str (:top dropline) "px")
                  :left (str (:left dropline) "px")}}
      (div {:class "horizontal"}))))
