(ns noven.dashboard.library
  (:require-macros [noven.dashboard.macros :refer [scroll]])
  (:require [noven.dashboard.util :as util]
            [noven.dashboard.viewer :refer [image-viewer]]
            [noven.dashboard.media :refer [media-viewer]]
            [noven.dashboard.inspector :refer [library-inspector]]
            [noven.dashboard.appstate :refer [app-state]]
            [noven.dashboard.uploader :refer [uploader-dropdown
                                              uploader-bar]]
            [noven.dashboard.dropdown :refer [toggle-dropdown
                                              open-dropdown
                                              close-dropdown]]
            [noven.dashboard.collections :refer [library-collections]]
            [noven.dashboard.library-modals :refer [visibility-modal]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span ul li a img h6 h5 input textarea]]
            [goog.dom :as dom]
            [goog.style :as style]
            [goog.events :as events]
            [goog.dom.dataset :as dataset]
            [goog.dom.classes :as classes]
            [goog.ui.Slider]
            [clojure.string :as string]
            [ajax.core :refer [GET POST PUT DELETE]]))

(defcomponent library-dock-slider
  [{:keys [library] :as props} owner]
  (render-state [_ {:keys [value] :as state}]
    (div {:class "slider"
          :ref "slider"}
      (div {:class "slider-track"})
      (div {:class "slider-handle"})))
  (did-mount [_]
    (let [slider (goog.ui.Slider.)
          steps 7
          get-value (fn []
                      (-> slider
                          (.getValue)
                          (/ 100)
                          (* steps)
                          (js/Math.round)))]
      (.setStep slider (/ 100 steps))
      (.decorate slider (om/get-node owner "slider"))
      (.addEventListener slider "change"
        (fn [_]
          (let [value (get-value)]
            (om/update! library :zoom value)))))))

(defcomponent library-dock-left [app owner]
  (render [_]
    (let [previous-tab (get-in app [:library :previous-tab])
          current-tab (get-in app [:library :tab])
          [previous-tab-name previous-location] (util/read-tab previous-tab)
          [current-tab-name current-location] (util/read-tab current-tab)]
      (div {:class "pull-left"}
        (when (not (or (= current-tab-name "timeline")
                       (and (= current-tab-name "collections")
                            (nil? current-location))))
          (let [[text url] (cond
                             (and (= current-tab-name "media")
                                  (nil? previous-tab-name))
                             ["Timeline" "#library"]

                             (and (= current-tab-name "media")
                                  (not (nil? previous-tab-name)))
                             ["Back" "#library"]

                             (and (= current-tab-name "collections")
                                  (not (nil? current-location)))
                             ["Collections" "#library/collections"])]
            (div {:class "nav-menu"}
              (span {:class "nav-menu-item back-button"}
                    (a {:href url} text)))))
        (when (or (= current-tab-name "timeline")
                  (and (= current-tab-name "collections")
                       (nil? current-location)))
          (om/build library-dock-slider (select-keys app [:library])))))))

(defcomponent library-dock-center [app owner]
  (render [_]
    (let [previous-tab (get-in app [:library :previous-tab])
          current-tab (get-in app [:library :tab])
          [previous-tab-name previous-location] (util/read-tab previous-tab)
          [current-tab-name current-location] (util/read-tab current-tab)]
      (div {:class "center"}
        (when (or (= current-tab-name "timeline")
                  (and (= current-tab-name "collections")
                       (nil? current-location)))
          (ul {:class "nav-group-buttons"}
              (li {:class (str "button " (if (= (get-in app [:library :tab]) :timeline) "active"))}
                  (a {:href "#library"} "Timeline"))
              (li {:class (str "button " (if (= (get-in app [:library :tab]) :collections) "active"))}
                  (a {:href "#library/collections"} "Collections"))))

        (when (and (= current-tab-name "media")
                   (not (empty? (:media app))))
          (let [medium (util/fetch (:media app) current-location)
                file (:file medium)
                title (or (:title medium) (:name file))]
            (div {:id "essay-title"
                  :class "center"}
              (div {:class "title"}
                title))))

        (when (and (= current-tab-name "collections")
                   (not (nil? current-location)))
          (div {:id "essay-title"
                :class "center"}
            (div {:class "title"}
              (condp = current-location
                "all-pictures" "All pictures"
                "last-import" "Last import"
                "trash" "Trash"))))))))

(defcomponent library-dock-right [app owner]
  (render [_]
    (let [previous-tab (get-in app [:library :previous-tab])
          current-tab (get-in app [:library :tab])
          [previous-tab-name previous-location] (util/read-tab previous-tab)
          [current-tab-name current-location] (util/read-tab current-tab)]
      (div {:class "pull-right"}
        (when (or (= current-tab-name "timeline")
                  (= current-tab-name "collections"))
          (ul {:class "nav-menu group-menu"}
              (li {:class "nav-menu-item dropdown-toggler"}
                  (span {:id "upload-button"
                         :style {:opacity (if (get-in app [:uploader :dropdown :open?]) 0.5 1)}
                         :on-click #(toggle-dropdown % (get-in app [:uploader :dropdown]))}
                        ;; (input {:id "upload-input"
                        ;;         :type "file"
                        ;;         :ref "fileinput"})
                        "Upload")
                  (om/build uploader-dropdown (select-keys app [:uploader
                                                                :library
                                                                :media])))
              (li {:class "nav-menu-item"}
                  (span {:style {:opacity (if (get-in app [:library :inspector :display]) 0.5 1)}
                         :on-click (fn [_]
                                     (let [display (get-in app [:library :inspector :display])]
                                       (om/update! app [:library :inspector :display]
                                                   (not display))))} "Inspector"))))))))

(defcomponent library-dock [app owner]
  (render [_]
    (div {:id "library-dock"
          :class "interface-dock"}
      (om/build library-dock-left app)

      (om/build library-dock-center app)

      (om/build library-dock-right app)))
  (did-mount [_])
  (will-update [_ new-props new-state]))

(defn load-media [{:keys [media] :as props} owner]
  (GET "/api/media"
      {:response-format :json
       :keywords? true
       :handler (fn [m]
                  (om/update! media (vec m)))}))

(defcomponent library-interface [{:keys [library media] :as props} owner]
  (render [_]
    (let [tab (:tab library)
          [tab-name location] (string/split (name tab) "#")]
      (div {:id "library-interface"
            :class (str "interface" (if (get-in props [:uploader :display]) " uploading"))}
        (om/build library-inspector props)

        (cond
          (= tab-name "timeline")
          (om/build media-viewer (select-keys props [:library
                                                     :media
                                                     :media-viewer]))

          (= tab-name "collections")
          (om/build library-collections (select-keys props [:library
                                                            :media
                                                            :media-viewer]))

          :else nil)

        (when (= tab-name "media")
          (om/build image-viewer (select-keys props [:library :media])))

        (om/build uploader-bar (select-keys props [:uploader]))

        (let [modal (condp = (get-in props [:library :modal])
                      :visibility visibility-modal
                      nil)]
          (if modal
            (om/build modal (select-keys props [:library
                                                :media])))))))
  (did-mount [_]
    (om/update! library :loading? true)
    (when (= (:tab library) :timeline)
      (load-media props owner)))
  (will-update [_ {new-library :library} _]
    (when (and (= (:tab new-library) :timeline)
               (not= (:tab library) :timeline))
      (load-media props owner))))
