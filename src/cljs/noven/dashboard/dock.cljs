(ns noven.dashboard.dock
  (:require [noven.dashboard.dropdown :refer [toggle-dropdown
                                              open-dropdown
                                              close-dropdown]]
            [noven.dashboard.library :refer [library-dock]]
            [noven.dashboard.text :refer [text-dock]]
            [noven.dashboard.site :refer [site-dock]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span ul li a input]]
            [goog.dom :as dom]
            [goog.style :as style]
            [goog.events :as events]
            [goog.dom.classes :as classes]))

(defcomponent navigator-dropdown [dropdown owner]
  (render [_]
    (div {:id "navigator-dropdown"
          :class "dropdown"
          :style {:display (if (:open? dropdown) "block" "none")
                  :left (str (:left dropdown) "px")
                  :top (str (:top dropdown) "px")}}
      (div {:class "triangle-with-shadow"})
      (div {:class "dropdown-menu"}
        (li {:class "menu-item"}
            (a {:class ""
                :href "#"} "Library"))
        (li {:class "menu-item"}
            (a {:class ""
                :href "#text"} "Text"))
        (li {:class "menu-item"}
            (a {:class ""
                :href "#site"} "Site")))))
  (did-mount [_]
    (let [dropdown-el (om/get-node owner)
          menu-items (dom/getElementsByClass "menu-item" dropdown-el)]
      (doseq [item menu-items]
        (events/listen item "click"
          (fn [_]
            (close-dropdown dropdown))))
      (events/listen dropdown-el "click" #(.stopPropagation %)))))

(defcomponent navigator [app owner]
  (render [_]
    (div {:id "navigator"}
      (span {:class "noven-logo"}
        "Noven")
      (span {:class "divide-dot"
             :style {:margin "0 3px"}} "∙")
      (span {:class "interface-name dropdown-toggler"
             :on-click #(toggle-dropdown % (:navigator-dropdown app))}
        (condp = (:interface app)
          :library "Library"
          :text "Text"
          :site "Site")
        (span {:class "caret"
               :style {:margin-left "4px"}})
        (om/build navigator-dropdown (:navigator-dropdown app))))))

(defcomponent dock [app owner]
  (render [_]
    (div {:id "dock"}
      (om/build navigator app)

      (om/build (condp = (:interface app)
                  :library library-dock
                  :text text-dock
                  :site site-dock) app))))
