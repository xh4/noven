(ns noven.dashboard.appstate)

(def document {:id 1
               :title "My Essay..."
               :blocks [{:type "paragraph"
                         :sym (keyword (gensym))
                         :text "The quick brown fox jumps over the lazy dog."
                         :markups {:em [[0 3]
                                        [31 34]]
                                   :strong [[4 9]
                                            [26 39]]}
                         :anchors [{:range [10 15]
                                    :url "#foo"}
                                   {:range [20 43]
                                    :url "#bar"}]}
                        {:type "list-item"
                         :sym (keyword (gensym))
                         :text "aaaaaa"
                         :markups {:em []
                                   :strong []}
                         :anchors []}
                        {:type "list-item"
                         :sym (keyword (gensym))
                         :text "bbbbbb"
                         :markups {:em []
                                   :strong []}
                         :anchors []}
                        {:type "list-item"
                         :sym (keyword (gensym))
                         :text "cccccc"
                         :markups {:em []
                                   :strong []}
                         :anchors []}
                        {:type "paragraph"
                         :sym (keyword (gensym))
                         :text "The quick brown fox jumps over the lazy dog."
                         :markups {:em [[0 3]
                                        [31 34]]
                                   :strong [[4 9]
                                            [26 39]]}
                         :anchors [{:range [10 15]
                                    :url "#"}
                                   {:range [20 43]
                                    :url "#"}]}
                        {:type "paragraph"
                         :sym (keyword (gensym))
                         :text "The quick brown fox jumps over the lazy dog."
                         :markups {:em [[0 3]
                                        [31 34]]
                                   :strong [[4 9]
                                            [26 39]]}
                         :anchors [{:range [10 15]
                                    :url "#"}
                                   {:range [20 43]
                                    :url "#"}]}]})

(def app-state
  (atom
   {:site {:logged-in? true}
    :interface :library
    :library {:selected []
              :current nil
              :loading? false
              :loaded []

              :tab :nil
              :previous-tab nil

              :modal nil

              :viewer {:display :hide}
              :inspector {:display false}}

    :collections []

    :uploader {:display false
               :files []
               :queue []
               :current-index nil
               :current-task nil
               :state :stopped
               :dropdown {:id "uploader-dropdown"
                          :style :pull-right
                          :open? false
                          :left 0
                          :top 0
                          :width 500}} ;; idle | working
    :media []
    :media-viewer {:rows []
                   :view-range [0 0]
                   :offset 0
                   :height 0
                   :width 0
                   :scrolling? false}
    :categories []
    :tags []
    :text {:tab :essays
           :previous-tab nil
           :loading? false
           :saving? false
           :modal nil
           :current nil}
    :essays []
    :editor {:field nil
             :document document ;; {:blocks []}
             :toolbar {:left 32
                       :top 32
                       :display true
                       :mode :text}
             :dropline {:top 0
                        :left 0
                        :display false}}
    :navigator-dropdown {:id "navigator-dropdown"
                         :open? false
                         :left 0
                         :top 0
                         :width 160}}))
