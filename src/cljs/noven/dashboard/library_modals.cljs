(ns noven.dashboard.library-modals
  (:require [noven.dashboard.util :as util]
            [noven.dashboard.appstate :refer [app-state]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span li a button h4 p]]
            [goog.dom :as dom]
            [goog.json :as json]
            [goog.events :as events]
            [goog.string :as gstring]
            [goog.dom.dataset :as dataset]
            [goog.dom.classes :as classes]
            [goog.string.format]
            [clojure.string :as string]
            [ajax.core :refer [GET POST PUT DELETE]]))

(defn close-modal [library-cursor]
  (om/update! library-cursor :modal nil))

(defn unpublish
  [library-cursor media-cursor media-entity]
  (let [id (:id media-entity)]
    (POST (str "/api/media/" id "/unpublish")
        {:format :raw
         :response-format :json
         :keywords? true
         :handler (fn [media-entity]
                    (close-modal library-cursor)
                    (util/update! media-cursor id media-entity))})))

(defcomponent visibility-modal
  [{:keys [library media] :as props}]
  (render [_]
    (let [current (first (:selected library))
          media-entity (util/fetch media current)]
      (div {:class "modal modal-dark"}
        (div {:class "modal-dialog"}
          (div {:class "modal-content"}
            (div {:class "modal-header"}
              (button {:type "button"
                       :class "close"
                       :on-click #(close-modal library)}
                      (span "×"))
              (h4 {:class "modal-title"} "Set as privite"))
            (div {:class "modal-body"}
              "Are you sure you want to set content \""
              (if (:title media-entity)
                (:title media-entity)
                (get-in media-entity [:file :name]))
              "\" as private?")
            (div {:class "modal-footer"}
              (button {:type "button"
                       :class "button button-dark"
                       :on-click #(close-modal library)}
                      "Cancel")
              (button {:type "button"
                       :class "button button-yellow"
                       :on-click #(unpublish library media media-entity)}
                      "Confirm"))))))))
