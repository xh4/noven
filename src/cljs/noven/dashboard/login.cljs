(ns noven.dashboard.login
  (:require [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros
             [div span ul ol li a
              form input button img label]]
            [ajax.core :refer [GET POST PUT DELETE]]
            [goog.events :as events]
            [goog.dom :as dom]))

(defcomponent ieno [app owner]
  (render [_]
    (div {:id "ieno"
          :style {:display "none"}}
      "This application is not compatible with this browser or version. Please use the latest version of Chrome, Safari, or Firefox.")))

(defn submit-login-form [e app owner]
  (.preventDefault e)
  (let [email (.-value (om/get-node owner "email"))
        password (.-value (om/get-node owner "password"))
        remember (.-checked (om/get-node owner "remember"))]
    (POST "/api/sessions" {:params {:email email
                                    :password password
                                    :remember remember}
                           :format :raw
                           :handler #(om/transact! app :logged-in? (fn [_] true))
                           :error-handler
                           (fn [{:keys [status]}]
                             (if (= status 401)
                               (om/transact! app :login-help (fn [_] true))))})))

(defcomponent reset-form [app owner]
  (render [_]
    (form {:id "reset_form"}
      (div {:class "field-container"} "Instructions will be emailed to you.")
      (div {:class "field-container"}
        (input {:type "email"
                    :class "field signin"
                    :placeholder "Email address"
                    :name "email"}))
      (div {:class "field-container"}
        (button {:type "submit"
                     :class "button c0"} "Reset password"))
      (div {:class "field-container"}
        (button {:class "button c3"} "Return to sign in")))))

(defcomponent login-help [app owner]
  (render [_]
    (div {:id "help"
          :style {:opacity (if (:login-help app) 1 0)}}
      (span {:class "no"}
        "Incorrect. Tey again or reset your password at site profile.")))
  (did-update [_ prev-props prev-state]
    (if (:login-help app)
      (js/setTimeout #(om/update! app [:login-help] false) 3000))))

(defcomponent login-form [app owner]
  (render-state [this state]
    (form {:id "login_form"}
      (div {:class "field-container"}
        (input {:id "email"
                    :type "email"
                    :class "field signin email"
                    :placeholder "Email address"
                    :name "email"
                    :autofocus ""
                    :ref "email"})
        (input {:type "password"
                    :class "field signin pass"
                    :placeholder "Password"
                    :name "password"
                    :ref "password"}))
      (div {:class "field-container"}
        (input {:name "remember"
                    :id "remember"
                    :type "checkbox"
                    :ref "remember"})
        (label {:for "remember"} "Keep me signed in"))
      (div {:class "field-container submit"}
        (button {:type "submit"
                     :id "login_submit"
                     :on-click #(submit-login-form % app owner)
                     :class "button c0"} "Sign in")))))

(defcomponent login-view [app owner]
  (render [_]
    (div {:id "login"}
      (div {:id "bg"})
      (om/build ieno app)
      (om/build login-help app)
      (div {:id "signin"}

        (div {:id "app-logo"}
          (img {:src "http://kevin1999.com/admin/images/app-logo-square.png"
                :width "72"
                :height "72"}))
        (div {:id "app-signin"}
          (om/build reset-form app)
          (om/build login-form app))))))
