(ns noven.dashboard.inspector
  (:require-macros [noven.dashboard.macros :refer [scroll col-row]])
  (:require [noven.dashboard.util :as util]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span li a h6 h5 input textarea]]
            [goog.date :as date]
            [goog.i18n.DateTimeFormat]
            [goog.events :as events]
            [ajax.core :refer [GET POST PUT DELETE]]))

(defn col-head [name]
  (div {:class "col-head"}
    (span {:class "in"}
          (a {:href "#"
              :class "arrow-toggle open fold"})
          (h6
           (a {:href "#"
               :class "fold"
               :title name} name)))))

(defn title-col-row [media id]
  (let [composition (atom false)]
    (input {:type "text"
            :class "field side-col"
            :ref "title"
            :on-compositionStart (fn [e]
                                   (reset! composition true))
            :on-compositionEnd (fn [e]
                                 (reset! composition false))
            :on-change (fn [e]
                         (let [value (.. e -target -value)]
                           (when-not @composition
                             (util/update! media id [:title] value)
                             (PUT (str "/api/media/" id)
                                 {:format :raw
                                  :params {:title value}}))))})))

(defn caption-col-row [media id]
  (let [composition (atom false)]
    (textarea {:class "field side-col expand tall"
               :row "4"
               :ref "caption"
               :on-compositionStart (fn [e]
                                      (reset! composition true))
               :on-compositionEnd (fn [e]
                                    (reset! composition false))
               :on-change (fn [e]
                            (let [value (.. e -target -value)]
                              (when-not @composition
                                (util/update! media id [:caption] value)
                                (PUT (str "/api/media/" id)
                                    {:format :raw
                                     :params {:caption value}}))))})))

(defcomponent collection-info
  [{:keys []} owner]
  (render [_]
    (div "Collection")))

(defcomponent collections-info
  [{:keys []} owner]
  (render [_]
    (div "Collections")))

(defcomponent timeline-info
  [{:keys []} owner]
  (render [_]
    (div "Timeline")))

(defcomponent selection-info
  [{:keys [library media categories tags] :as props} owner]
  (render [_]
    (div "Selection")))

(defn publish-media
  [media-cursor media-entity]
  (POST (str "/api/media/" (:id media-entity) "/publish")
      {:format :raw
       :response-format :json
       :keywords? true
       :handler (fn [media-entity]
                  (util/update! media-cursor (:id media-entity) media-entity))}))

(defcomponent media-info
  [{:keys [library media categories tags] :as props} owner]
  (render [_]
    (let [selected-id (first (:selected library))
          media-entity (util/fetch media selected-id)
          file (:file media-entity)
          metadata (:metadata file)
          content (:content media-entity)
          content-categories (:categories content)
          category (first content-categories)
          content-tags (:tags content)]

      (when media-entity
        (div {}
          (div {:class "col-block"}
            (col-head "Properties")
            (div {:class "col-group"}
              (col-row "File" (:name file))
              (col-row "Size" (:size file))
              (col-row "Dimensions" (util/format "%s x %s" (:width metadata)
                                                 (:height metadata)))
              (col-row "ID" (:id media-entity))
              (col-row "Title"
                       (title-col-row media selected-id))
              (col-row "Caption"
                       (caption-col-row media selected-id))))

          (div {:class "col-block"}
            (col-head "Publication")
            (if-not (:content media-entity)
              (div {:class "col-group"}
                (col-row "Visibility"
                         (span {:class "col-value"}
                               "Private"))
                (div {:class "col-row button-row"}
                  (div {:class "button button-xs button-yellow"
                        :on-click #(publish-media media media-entity)}
                    "Publish")))
              (div {:class "col-group"}
                (col-row "Visibility"
                         (span {:class "col-value"}
                               "Public")
                         (a {:href "#"
                             :title "Edit visibility"
                             :class "edit-link"
                             :on-click #(om/update! library :modal :visibility)} "edit"))
                (col-row "Categories"
                         (span {:class "col-value"}
                               "- none -")
                         (a {:href "#"
                             :title "Edit categories"
                             :class "edit-link"
                             :on-click #(.preventDefault %)} "edit"))
                (col-row "Tags"
                         (if (empty? tags)
                           (span {:class "col-value"}
                                 "- none -")
                           (span {:class "tag-display"}
                                 (for [tag tags]
                                   (a {:href "#"
                                       :title (str "View all media tagged " (:name tag))
                                       :class "data-link tag"
                                       :on-click #(.preventDefault %)}
                                      (:name tag)))))
                         (a {:href "#"
                             :title "Edit tags"
                             :class "edit-link"
                             :data-off-canvas "tags"}
                            "edit"))
                )))

          (div {:class "col-block"}
            (col-head "Ownership")
            (div {:class "col-group"}
              (col-row "License" "© All rights reserved")
              (col-row "Download" "None")))

          (div {:class "col-block"}
            (col-head "Date")
            (let [formatter (goog.i18n.DateTimeFormat. "yyyy-MM-dd hh:mm")]
              (div {:class "col-group"}
                (col-row "Uploaded" (.format formatter (goog.date/fromIsoString (:created_at media-entity))))
                (when (:content media-entity)
                  (col-row "Published" (.format formatter (goog.date/fromIsoString (:published_at media-entity))))))))))))
  (did-mount [_]
    (let [selected-id (first (:selected library))
          media-entity (util/fetch media selected-id)
          title-input (om/get-node owner "title")
          caption-textarea (om/get-node owner "caption")]
      (aset title-input "value" (:title media-entity))
      (aset caption-textarea "value" (:caption media-entity))))
  (did-update [_ _ _]
    (let [selected-id (first (:selected library))
          media-entity (util/fetch media selected-id)
          title-input (om/get-node owner "title")
          caption-textarea (om/get-node owner "caption")]
      (aset title-input "value" (:title media-entity))
      (aset caption-textarea "value" (:caption media-entity)))))

(defcomponent library-inspector
  [{:keys [library media categories tags] :as props} owner]
  (render [_]
    (div {:id "library-inspector"
          :class "inspector inspector-dark"
          :style {:display (if (get-in library [:inspector :display]) "block" "none")}}
      (div {:class "inspector-header"}
        (span {:class "inspector-close-icon"
               :on-click #(om/update! library [:inspector :display] false)})
        (h5 {:class "inspector-title"} "Inspector"))
      (div {:class "inspector-body"}
        (scroll
         (let [selected (:selected library)]
           (cond
             (= (count selected) 1)
             (om/build media-info
                       (select-keys props [:library
                                           :media
                                           :categories
                                           :tags]))
             :else nil)))))))
