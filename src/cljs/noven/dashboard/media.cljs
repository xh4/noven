(ns noven.dashboard.media
  (:require-macros [noven.dashboard.macros :refer [scroll]])
  (:require [noven.dashboard.util :as util]
            [noven.dashboard.media-entity :refer [media-entity]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span ul li a]]
            [goog.dom :as dom]
            [goog.style :as style]
            [goog.events :as events]
            [goog.dom.dataset :as dataset]
            [goog.dom.classes :as classes]
            [clojure.string :as string]))

(defcomponent media-viewer
  [{:keys [library media] :as props} owner]
  (render-state [_ state]
    (div {:id "media-viewer"
          :style {:right (if (get-in library [:inspector :display]) "280px" 0)}
          :on-click (fn [_]
                      (om/update! library [:selected] []))}
      (div {:id "media"}
        (scroll
         (for [m media]
           (om/build media-entity {:media-entity m
                                   :library library
                                   :media media})))))))
