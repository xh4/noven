(ns noven.dashboard.app
  (:require [noven.dashboard.dock :refer [dock]]
            [noven.dashboard.library :refer [library-interface]]
            [noven.dashboard.text :refer [text-interface]]
            [noven.dashboard.site :refer [site-interface]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span li a h6 h5 input textarea]]))


(defcomponent app-view [app owner]
  (render [_]
    (div {:id "app"}
      (om/build dock app)
      (om/build (condp = (:interface app)
                  :library library-interface
                  :text text-interface
                  :site site-interface) app)
      (div {:id "dropdown-backdrop"}))))
