(ns noven.dashboard.dropdown
  (:require [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span ul li a]]
            [goog.dom :as dom]
            [goog.style :as style]
            [goog.events :as events]
            [goog.dom.classes :as classes]))

(defn close-dropdown [dropdown-cursor]
  (om/update! dropdown-cursor [:open?] false)
  (-> (dom/$ "dropdown-backdrop")
      (classes/remove "active")))

(defn open-dropdown [dropdown-cursor]
  (om/update! dropdown-cursor [:open?] true)
  (-> (dom/$ "dropdown-backdrop")
      (classes/add "active"))
  (events/listenOnce (dom/$ "dropdown-backdrop") "click"
    #(close-dropdown dropdown-cursor)))

(defn toggle-dropdown [e dropdown-cursor]
  (let [open? (:open? dropdown-cursor)
        trigger (dom/getAncestorByClass (.-target e) "dropdown-toggler")
        dropdown (dom/getElement (:id dropdown-cursor))
        caret-height 15
        trigger-size (style/getBorderBoxSize trigger)
        left (/ (- (.-width trigger-size) (:width dropdown-cursor)) 2)
        top (+ (.-height trigger-size) caret-height -10)
        left (if (= (:style dropdown-cursor) :pull-right)
               (- (.-width trigger-size) (:width dropdown-cursor)
                  (- (/ 62 2)))
               left)]
    (om/transact! dropdown-cursor #(merge % {:left left
                                             :top top}))

    (open-dropdown dropdown-cursor)))
