(ns noven.dashboard.ui
  (:require-macros [noven.dashboard.macros :refer [scroll col-row]])
  (:require [noven.dashboard.dock :refer [dock]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span li a h6 h5 input textarea]]))

(defn col-head [name]
  (div {:class "col-head"}
    (span {:class "in"}
          (a {:href "#"
              :class "arrow-toggle open fold"})
          (h6
           (a {:href "#"
               :class "fold"
               :title name} name)))))

(defcomponent ui-view [app owner]
  (render [_]
    (div {:id "app"}
      (om/build dock app)
      (div {:class "interface library-interface"})
      (div {:class "dropdown"}
        (div {:class "triangle-with-shadow"})
        (div {:class "dropdown-menu"}
          (li (a {:class ""} "上传演示文稿..."))
          (li {:class "divider"
               :role "separator"})
          (li {:class "disabled"} (a {:class ""} "下载文稿..."))))
      (a {:class "button button-sm"
          :style {:top "50px"
                  :left "30px"
                  :background-color "#ffd455"}} "Upload")


      (div {:class "inspector inspector-light"
            :style {:left "240px"
                    :top "75px"}}
        (div {:class "inspector-header"}
          (span {:class "inspector-close-icon"})
          (h5 {:class "inspector-title"} "Inspector"))
        (div {:class "inspector-body"}
          (div {:class "col-block"}
            (col-head "Properties")
            (div {:class "col-group"}
              (col-row "File" "6901481-guilty-crown.jpg")
              (col-row "Size" "4.2 MB")
              (col-row "Dimensions" "1920 x 1080")
              (col-row "ID" 14)
              (col-row "Title"
                       (input {:type "text"
                               :class "field side-col"}))
              (col-row "Caption"
                       (textarea {:class "field side-col expand tall"
                                  :rows 4}))))
          (div {:class "col-block"}
            (col-head "Publication")
            (div {:class "col-group"}
              (col-row "Categories"
                       (span {:class "col-value"}
                             "- none -")
                       (a {:href "#"
                           :title "Edit categories"
                           :class "edit-link"} "edit"))))))


      (div {:class "inspector inspector-dark"
            :style {:left "550px"
                    :top "75px"}}
        (div {:class "inspector-header"}
          (span {:class "inspector-close-icon"})
          (h5 {:class "inspector-title"} "Inspector"))
        (div {:class "inspector-body"}
          (div {:class "col-block"}
            (col-head "Properties")
            (div {:class "col-group"}
              (col-row "File" "6901481-guilty-crown.jpg")
              (col-row "Size" "4.2 MB")
              (col-row "Dimensions" "1920 x 1080")
              (col-row "ID" 14)
              (col-row "Title"
                       (input {:type "text"
                               :class "field side-col"}))
              (col-row "Caption"
                       (textarea {:class "field side-col expand tall"
                                  :rows 4}))))
          (div {:class "col-block"}
            (col-head "Publication")
            (div {:class "col-group"}
              (col-row "Categories"
                       (span {:class "col-value"}
                             "- none -")
                       (a {:href "#"
                           :title "Edit categories"
                           :class "edit-link"} "edit"))))))


      (div {:id "upload-bar"}
        (span {:class "spinner active"}
              (span {:class "spinner-circle"}))
        (span {:class "upload-status"}
              "正在上传 19 个项目...")
        (span {:class "progress-bar"}
              (span {:class "fill"}))
        (span {:class "cancel-button"}
              "停止上传")))))
