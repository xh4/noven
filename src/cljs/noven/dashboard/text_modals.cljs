(ns noven.dashboard.text-modals
  (:require [noven.dashboard.util :as util]
            [noven.dashboard.appstate :refer [app-state]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span li a button h4 p]]
            [goog.dom :as dom]
            [goog.json :as json]
            [goog.events :as events]
            [goog.string :as gstring]
            [goog.dom.dataset :as dataset]
            [goog.dom.classes :as classes]
            [goog.string.format]
            [clojure.string :as string]
            [ajax.core :refer [GET POST PUT DELETE]]))

(defn close-modal [text-cursor]
  (om/update! text-cursor :modal nil))

(defn create-essay
  [text-cursor essays-cursor]
  (POST "/api/essays"
      {:response-format :json
       :keywords? true
       :handler (fn [essay]
                  (close-modal text-cursor)
                  (om/transact! essays-cursor
                    #(conj % essay)))}))

(defcomponent create-modal
  [{:keys [text essays] :as props} owner]
  (render [_]
    (let [current (:current text)
          essay (util/fetch essays current)]
      (div {:class "modal"}
        (div {:class "modal-dialog"}
          (div {:class "modal-content"}
            (div {:class "modal-header"}
              (button {:type "button"
                       :class "close"
                       :on-click #(close-modal text)}
                      (span "×"))
              (h4 {:class "modal-title"} "New essay"))
            (div {:class "modal-body"}
              )
            (div {:class "modal-footer"}
              (button {:type "button"
                       :class "button button-light"
                       :on-click #(close-modal text)}
                      "Cancel")
              (button {:type "button"
                       :class "button button-blue"
                       :on-click #(create-essay text essays)}
                      "Create"))))))))

(defn delete-essay
  [text-cursor essays-cursor essay]
  (DELETE (str "/api/essays/" (:id essay))
      {:response-format :raw
       :handler (fn [_]
                  (close-modal text-cursor)
                  (util/delete! essays-cursor (:id essay)))}))

(defcomponent delete-modal
  [{:keys [text essays] :as props} owner]
  (render [_]
    (let [current (:current text)
          essay (util/fetch essays current)]
      (div {:class "modal"}
        (div {:class "modal-dialog"}
          (div {:class "modal-content"}
            (div {:class "modal-header"}
              (button {:type "button"
                       :class "close"
                       :on-click #(close-modal text)}
                      (span "×"))
              (h4 {:class "modal-title"} "Delete essay"))
            (div {:class "modal-body"}
              (p (gstring/format "Are you sure you want to delete the essay \"%s\"?" (:title essay))))
            (div {:class "modal-footer"}
              (button {:type "button"
                       :class "button button-light"
                       :on-click #(close-modal text)}
                      "Cancel")
              (button {:type "button"
                       :class "button button-blue"
                       :on-click #(delete-essay text essays essay)}
                      "Delete"))))))))

(defn publish-essay
  [text-cursor essays-cursor essay]
  (POST (str "/api/essays/" (:id essay) "/publish")
      {:format :raw
       :response-format :json
       :keywords? true
       :handler (fn [essay]
                  (close-modal text-cursor)
                  (util/update! essays-cursor (:id essay) essay))}))

(defcomponent publish-modal
  [{:keys [text essays] :as props} owner]
  (render [_]
    (let [current (:current text)
          essay (util/fetch essays current)]
      (div {:class "modal"}
        (div {:class "modal-dialog"}
          (div {:class "modal-content"}
            (div {:class "modal-header"}
              (button {:type "button"
                       :class "close"
                       :on-click #(close-modal text)}
                      (span "×"))
              (h4 {:class "modal-title"} "Publish essay"))
            (div {:class "modal-body"}
              (p (gstring/format "Are you sure you want to publish the essay \"%s\"?" (:title essay))))
            (div {:class "modal-footer"}
              (button {:type "button"
                       :class "button button-light"
                       :on-click #(close-modal text)}
                      "Close")
              (button {:type "button"
                       :class "button button-blue"
                       :on-click #(publish-essay text essays essay)}
                      "Publish"))))))))

(defcomponent categories-modal
  [{:keys [] :as props} owner]
  (render [_]))

(defcomponent tags-modal
  [{:keys [] :as props} owner]
  (render [_]))
