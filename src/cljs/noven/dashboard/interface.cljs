(ns noven.dashboard.interface
  (:require [noven.dashboard.appstate :refer [app-state]]
            [goog.dom :as dom]
            [goog.style :as style]
            [goog.events :as events]
            [goog.dom.classes :as classes]
            [domina.css :refer [sel]]
            [domina :refer [nodes single-node]]))

(defn on-window-resize [e]
  (when (= (:library-view @app-state) :single)
    (let [mid-single-wrap (.getElementById js/document "mid-single-wrap")
          width (.-offsetWidth mid-single-wrap)
          height (.-offsetHeight mid-single-wrap)
          max-width (- width 30)
          max-height (- height 30)])))

(defn listen-window-resize []
  (events/removeAll js/window)
  (events/listen js/window "resize" on-window-resize))

(defn sync-scroll [wrapper]
  (let [scroll-content (dom/getElementByClass "scroll_content" wrapper)
        scroll-bar (dom/getElementByClass "scroll_bar" wrapper)
        track-footer (dom/getElementByClass "scroll_track_footer" wrapper)
        content-height (.-height (style/getSize scroll-content))
        wrapper-height (.-height (style/getSize wrapper))
        d (/ wrapper-height content-height)
        grab-height (* d wrapper-height)]
    (if (<= content-height wrapper-height)
      (do
        (style/setStyle scroll-bar "display" "none")
        (classes/add track-footer "scroll_track_off"))
      (do
        (style/setStyle scroll-bar "display" "block")
        (classes/remove track-footer "scroll_track_off")
        (style/setHeight scroll-bar (str (* d 100) "%"))
        (let [scroll-top (.-scrollTop wrapper)
              d (/ scroll-top content-height)]
          (style/setStyle scroll-bar "top" (str (* d 100) "%")))))))

(defn hook-scrollbar [wrapper]
  (events/removeAll wrapper)
  (events/listen wrapper "scroll" #(sync-scroll wrapper)))

(defn start-hook-scrollbars []
  (js/setInterval
   #(let [wrappers (dom/getElementsByClass "scroll_wrapper")]
      (doseq [wrapper wrappers]
        (hook-scrollbar wrapper)))
   100))
