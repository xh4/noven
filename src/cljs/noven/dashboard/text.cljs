(ns noven.dashboard.text
  (:require [noven.dashboard.util :as util]
            [noven.dashboard.appstate :refer [app-state]]
            [noven.dashboard.editor :as editor :refer [editor-view]]
            [noven.dashboard.essay :refer [essays-view
                                           essays-empty-state]]
            [noven.dashboard.text-modals :refer [create-modal
                                                 delete-modal
                                                 publish-modal
                                                 categories-modal
                                                 tags-modal]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span li a img h6 h5 input textarea]]
            [goog.dom :as dom]
            [goog.json :as json]
            [goog.style :as style]
            [goog.events :as events]
            [goog.string :as gstring]
            [goog.dom.dataset :as dataset]
            [goog.dom.classes :as classes]
            [goog.string.format]
            [clojure.string :as string]
            [ajax.core :refer [GET POST PUT DELETE]]))

(defn without-sym
  "Remove all `block symbol` from blocks."
  [blocks]
  (vec (map (fn [block]
              (select-keys block (remove #(= % :sym) (keys block)))) blocks)))

(defn serialize-blocks
  "Turn blocks into JSON string."
  [blocks]
  (-> blocks
      without-sym
      clj->js
      json/serialize))

(defn with-sym
  "Given blocks, return blocks with symbols associated."
  [blocks]
  (vec (map #(assoc % :sym (keyword (gensym))) blocks)))

(defn read-essay
  "Read essay JSON returned from server."
  [essay]
  (-> essay
      (update-in [:document] with-sym)
      (update-in [:draft] with-sym)))

(defn save-essay [essays-cursor]
  (let [document (editor/get-document)]
    (PUT (str "/api/essays/" (:id document))
        {:format :raw
         :params {:title (:title document)
                  :document (serialize-blocks (:blocks document))
                  :draft (serialize-blocks (:blocks document))}
         :keywords? true
         :response-format :json
         :handler (fn [essay]
                    (util/update! essays-cursor (:id essay) (read-essay essay))
                    (swap! editor/document assoc :last-update-time (goog.now)))})))

(defcomponent save-button [{:keys [text essays] :as props} owner]
  (init-state [_]
    {:changed false})
  (render [_]
    (div {:class "nav-menu"}
      (span {:class (str "nav-menu-item"
                         (if-not (om/get-state owner :changed) " disabled"))
             :on-click #(save-essay essays)}
            (span "Save"))))
  (did-mount [_]
    (add-watch editor/document :watcher
      (fn [key atom old-state new-state]
        (let [id (:id new-state)
              essay (util/fetch essays id)
              draft (:draft essay)]
          (if (and (= (without-sym draft)
                      (without-sym (:blocks new-state)))
                   (= (:title essay)
                      (:title new-state)))
            (om/set-state! owner :changed false)
            (om/set-state! owner :changed true))))))
  (will-unmount [_]
    (remove-watch editor/document :watcher)))

(defcomponent text-dock [{:keys [text essays] :as props} owner]
  (render [_]
    (div {:id "text-dock"
          :class "interface-dock"}
      (div {:class "pull-left"}
        (if (= (:tab text) :editor)
          (div {:class "nav-menu"}
            (span {:class "nav-menu-item back-button"
                   :on-click #(om/update! text :tab :essays)}
                  (span "Back")))))
      (div {:class "center"})
      (div {:class "pull-right"}
        (if (= (:tab text) :editor)
          (om/build save-button (select-keys props [:text :essays])))
        (if (= (:tab text) :essays)
          (div {:class "nav-menu"}
            (span {:class "nav-menu-item"
                   :on-click #(om/update! text :modal :create)}
                  (span "New"))))))))

(defcomponent text-interface [props owner]
  (render [_]
    (div {:id "text-interface"
          :class "interface"}
      (if (= (get-in props [:text :tab]) :essays)
        (if (empty? (:essays props))
          (om/build essays-empty-state {})
          (om/build essays-view props)))
      (if (= (get-in props [:text :tab]) :editor)
        (om/build editor-view (select-keys props [:editor])))
      (let [modal (condp = (get-in props [:text :modal])
                    :create create-modal
                    :delete delete-modal
                    :publish publish-modal
                    :categories categories-modal
                    :tags tags-modal
                    nil)]
        (if modal
          (om/build modal (select-keys props [:text
                                              :essays]))))))
  (did-mount [_]
    (GET "/api/essays"
        {:response-format :json
         :keywords? true
         :handler (fn [essays]
                    (om/update! props :essays
                                (mapv read-essay essays)))})))
