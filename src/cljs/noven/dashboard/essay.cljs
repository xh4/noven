(ns noven.dashboard.essay
  (:require [noven.dashboard.util :as util]
            [noven.dashboard.appstate :refer [app-state]]
            [noven.dashboard.editor :as editor]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span li a]]
            [goog.dom :as dom]
            [goog.json :as json]
            [goog.style :as style]
            [goog.events :as events]
            [goog.string :as gstring]
            [goog.date]
            [goog.i18n.DateTimeFormat]
            [goog.dom.dataset :as dataset]
            [goog.dom.classes :as classes]
            [goog.string.format]
            [clojure.string :as string]
            [ajax.core :refer [GET POST PUT DELETE]]))

(defn format-date [s]
  (let [d (goog.date/fromIsoString s)
        f (goog.i18n.DateTimeFormat. "MMM dd yyyy")]
    (when d (.format f d))))

(defcomponent essays-empty-state
  [{:keys [] :as props} owner]
  (render [_]
    (div {:style {:color "#919191"
                  :position "fixed"
                  :left "50%"
                  :top "50%"
                  :margin-left "-60px"
                  :margin-top "-10px"}}
      "No contents yet...")))

(defcomponent essays-view [{:keys [text essays] :as props} owner]
  (render [_]
    (div {:class "entries"}
      (for [essay essays]
        (div {:class (str "entry" (if (:cover essay) " has-cover"))
              :on-click #(om/update! text :current (:id essay))}
          (div {:class "entry-title"}
            (if (empty? (:title essay))
              "Untitled"
              (:title essay)))
          (div {:class "entry-state"}
            (condp = (:state essay)
              "edited" "Edited"
              "draft" "Draft"
              ""))
          (if (:cover essay)
            (div {:class "entry-cover"}))
          (div {:class "entry-excerpt"}
            (:excerpt essay))
          ;; (div {:class "entry-pictures"}
          ;;   (div {:class "entry-picture"})
          ;;   (div {:class "entry-picture"})
          ;;   (div {:class "entry-picture"})
          ;;   (div {:class "entry-picture"}))
          (div {:class "entry-footer"}
            (span {:class "group"}
                  (span {:class "btn"
                         :on-click #(do
                                      (editor/set-document {:id (:id essay)
                                                            :title (:title essay)
                                                            :blocks (:document essay)})
                                      (om/update! text :tab :editor))}
                        "Edit")
                  (span {:style {:padding "0 5px"}} "∙")
                  (span {:class "btn"
                         :on-click #(om/update! text :modal :delete)}
                        "Delete")
                  (if (= (:state essay) "draft")
                    (span
                     (span {:style {:padding "0 5px"}} "∙")
                     (span {:class "btn"
                            :on-click #(om/update! text :modal :publish)}
                           "Publish"))
                    (span
                     (span {:style {:padding "0 5px"}} "∙")
                     (span {:class "btn"
                            :on-click #(om/update! text :modal :publish)}
                           "Status"))))
            (span {:class "sep"}
                  "|")
            (if (or (= (:state essay) "public")
                    (= (:state essay) "edited"))
              (span
               (span {:class "group"}
                     (span {:class "btn"
                            :on-click #(om/update! text :modal :categories)}
                           "- Category -"))
               (span {:class "sep"}
                     "|")
               (span {:class "group"}
                     (span {:class "btn"
                            :on-click #(om/update! text :modal :tags)}
                           "- Tag -"))
               (span {:class "sep"}
                     "|")))
            (span {:class "group"}
                  (span {:class "btn"}
                        (let [date (condp = (:state essay)
                                     "public" (:published_at essay)
                                     "edited" (:updated_at essay)
                                     "draft" (:updated_at essay))]
                          (format-date date))))))))))
