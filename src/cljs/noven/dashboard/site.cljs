(ns noven.dashboard.site
  (:require [noven.dashboard.appstate :refer [app-state]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span li a img]]
            [goog.dom :as dom]
            [goog.style :as style]
            [goog.events :as events]
            [goog.dom.dataset :as dataset]
            [goog.dom.classes :as classes]))

(defcomponent site-dock [props owner]
  (render [_]
    (div {:id "site-dock"
          :class "interface-dock"})))

(defcomponent site-interface [props owner]
  (render [_]
    (div {:id "site-interface"
          :class "interface"
          :style {:padding "12px"}}
      "Site interface")))
