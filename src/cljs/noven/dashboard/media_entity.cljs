(ns noven.dashboard.media-entity
  (:require [noven.dashboard.util :as util]
            [noven.dashboard.appstate :refer [app-state]]
            [clojure.string :as string]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros
             [div span ul ol li a img]]
            [goog.events :as events]
            [goog.date :as date]
            [goog.dom :as dom]
            [goog.i18n.DateTimeFormat]))

(defn format-date [s]
  (when s
    (let [d (goog.date/fromIsoString s)
          f (goog.i18n.DateTimeFormat. "MMM dd yyyy")]
      (when d (.format f d)))))

(defn selected?
  [library media-entity]
  (util/vec-contains? (:selected library) (:id media-entity)))

(defn on-single-click-media-entity
  [e media-entity library]
  ;; altKey
  ;; ctrlKey
  ;; metaKey
  ;; shiftKey
  (let [id (:id @media-entity)
        selected? (selected? @library @media-entity)
        metakey? (.-metaKey e)
        ctrlkey? (.-ctrlKey e)
        shiftkey? (.-shiftKey e)
        multiselect? (or metakey? ctrlkey?)]
    ;; only
    ;; add
    ;; remove
    (cond
      selected? (cond
                  multiselect? (om/transact! library :selected (fn [v]
                                                                 (vec (remove #(= % id) v))))
                  :else (om/update! library :selected [id]))
      :else (cond
              multiselect? (om/transact! library :selected #(conj % id))
              :else (om/update! library :selected [id])))))

(defn on-double-click-media-entity
  [e media-entity library]
  (om/update! library :loading? true)
  (om/update! library :current (:id media-entity))
  (aset js/window "location" "hash" (str "#library/media/" (:id media-entity))))

(defn on-click-media-entity
  [e media-entity library]
  (let [metakey? (.-metaKey e)
        ctrlkey? (.-ctrlKey e)
        shiftkey? (.-shiftKey e)
        multiselect? (or metakey? ctrlkey?)]
    (if (or shiftkey? multiselect?) (on-single-click-media-entity e media-entity library)
        (condp = (.. e -detail)
          1 (on-single-click-media-entity e media-entity library)
          2 (on-double-click-media-entity e media-entity library)
          3 nil)))
  (.preventDefault e)
  (.stopPropagation e))

(defcomponent media-entity
  [{:keys [media-entity library media]} owner]
  (init-state [_]
    {:loaded (util/vec-contains? (:loaded library) (:id media-entity))})
  (did-mount [_]
    (when-not (util/vec-contains? (:loaded library) (:id media-entity))
      (let [file (:file media-entity)
            img (js/Image.)]
        (set! (.-onload img)
              (fn []
                (om/transact! library :loaded (fn [v] (conj v (:id media-entity))))
                (om/set-state! owner :loaded true)))
        (set! (.-src img) (util/make-url file "small")))))
  (render-state [_ state]
    (let [file (:file media-entity)
          {:keys [width height]} (:metadata file)
          zoom (:zoom library)
          box-size (+ 100 (* 20 zoom))
          {left :left
           top :top
           display-width :width
           display-height :height} (util/fit-size box-size
                                                  box-size
                                                  width
                                                  height)]
      (div {:class (str "media-entity "
                        (if (selected? library media-entity) "selected ")
                        (if (:loaded state) "loaded" "loading"))
            :style {:width (str box-size "px")}}
        (span {:class "grid-data"}
              (span {:class "content"
                     :style {:width (str box-size "px")
                             :height (str box-size "px")}}
                    (a {:class "img-wrap"
                        :href (str "#library/media/" (:id media-entity))
                        :on-click #(on-click-media-entity % media-entity library)
                        :style {:width (str display-width "px")
                                :height (str display-height "px")
                                :position "absolute"
                                :left (str left "px")
                                :top (str top "px")}}
                       (when (:loaded state)
                         (img {:width display-width
                               :height display-height
                               :src (util/make-url file "small")
                               :style {:display "inline"
                                       :position "absolute"
                                       :left 0
                                       :top 0}}))))
              (span {:class "title filename"
                     :style {:display "block"}} (if (empty? (:title media-entity))
                                                  (:name file)
                                                  (:title media-entity))))))))
