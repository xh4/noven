(ns noven.dashboard.modal
  (:require [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span ul li a]]))

(defcomponent modal
  [{:keys [] :as props} owenr]
  (render [_]
    (div {:class "modal"}
      (div {:class "modal-box modal-box-dark"
            :style {:width "500px"
                    :height "500px"
                    :margin-left "-250px"
                    :margin-top "-250px"}}
        (div {:class "modal-box-header"}
          (div {:class "modal-box-close-icon"}))
        (div {:class "modal-box-body"})))))
