(ns noven.dashboard.uploader
  (:require [noven.dashboard.util :as util]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [div span li a input h3 p button]]
            [goog.dom :as dom]
            [goog.style :as style]
            [goog.events :as events]
            [goog.dom.classes :as classes])
  (:import [goog.net XhrIo]
           goog.net.EventType
           [goog.events EventType FileDropHandler]))

(defn handle-queue [uploader library queue]
  (cond
    (empty? queue) (do
                     (om/transact! uploader
                       #(merge % {:display false
                                  :state :idle
                                  :current-index nil
                                  :current-task nil
                                  :queue []}))
                     (om/update! library :selected [])
                     (om/update! uploader [:dropdown :open?] false)
                     (aset js/window "location" "hash" "#library/collections/last-import"))
    :else (let [task (first queue)
                file (:file task)
                xhr (goog.net.XhrIo.)
                formdata (js/FormData.)]
            (om/transact! uploader :current-index (fn [index]
                                                    (cond
                                                      (nil? index) 0
                                                      :else (inc index))))
            (om/update! uploader :current-task task)
            (goog.events/listenOnce xhr (.. goog.net -EventType -COMPLETE)
              (fn [e]
                (om/transact! uploader :queue
                  (fn [q]
                    (vec (map
                          (fn [t]
                            (if (= (:file t) file)
                              (merge t {:progress 100})
                              t))
                          q))))
                (handle-queue uploader library (rest queue))))
            (goog.events/listen xhr (.. goog.net -EventType -PROGRESS)
              (fn [e]
                (.log js/console e)))
            (.append formdata "file" file)
            (when (:head task)
              (.append formdata "head" "true"))
            (.send xhr "/api/media"
                   "POST"
                   formdata))))

(defn make-task [file head?]
  {:file file
   :progress 0
   :head head?})

(defn start-upload [{:keys [uploader
                            library
                            media] :as props}]
  (om/update! uploader :display true)
  (let [files (:files uploader)
        queue (mapv
               (fn [idx file]
                 (let [head? (zero? idx)]
                   (make-task file head?)))
               (range (count files))
               files)]
    (om/update! uploader :queue queue)
    (om/update! uploader :files [])
    (om/update! uploader :state :working)
    (handle-queue uploader library queue)))

(defn total-progress [queue]
  (reduce
   (fn [total task]
     (+ total (/ (:progress task) (count queue))))
   0
   queue))

(defn on-files-added [uploader-cursor files]
  (doseq [idx (range (.-length files))]
    (let [file (.item files idx)]
      (om/transact! uploader-cursor :files #(conj % file)))))

(defcomponent uploader-bar [{:keys [uploader] :as props} owner]
  (render [_]
    (div {:id "uploader-bar"}
      (span {:class "spinner active"}
            (span {:class "spinner-circle"}))
      (span {:class "uploader-status"}
            (util/format "Now uploading %s items..." (count (:queue uploader))))
      (span {:class "progress-bar"}
            (let [percent (total-progress (:queue uploader))]
              (div {:class "fill"
                    :style {:width (str percent "%")}})))
      (span {:class "cancel-button"}
            "Stop"))))

(defcomponent dropzone [{:keys [uploader]} owner]
  (render [_]
    (let [files-count (count (:files uploader))]
      (div {:class "dropzone"
            :ref "dropzone"}
        (if (zero? files-count)
          (div {:class "inner"}
            (h3 "Drop files here or click to brower")
            (p "JPG, PNG, GIF, MP4"))
          (div {:class "inner"}
            (h3 (span {:class "label-success-select"}
                      (span {:id "upload_queued_num"} files-count)
                      " items queued"))
            (p "Add more or click Import to begin upload")))
        (input {:type "file"
                :multiple "true"
                :ref "fileinput"
                :style {:position "absolute"
                        :display "block"
                        :left "0"
                        :top "0"
                        :width "100%"
                        :height "100%"
                        :opacity "0"
                        :cursor "copy"}}))))
  (did-mount [_]
    (let [dropzone (om/get-node owner "dropzone")
          fileinput (om/get-node owner "fileinput")
          drop-handler (goog.events.FileDropHandler. dropzone true)]
      (events/listen drop-handler goog.events.FileDropHandler.EventType.DROP
        (fn [e]
          (let [files (.. e (getBrowserEvent) -dataTransfer -files)]
            (on-files-added uploader files))))
      (events/listen fileinput "change" (fn [e]
                                          (let [files (.. e -target -files)]
                                            (on-files-added uploader files)))))))

(defcomponent uploader-dropdown [{:keys [uploader
                                         library
                                         media] :as props} owner]
  (render [_]
    (let [dropdown (:dropdown uploader)]
      (div {:id "uploader-dropdown"
            :class "dropdown pull-right dropdown-panel"
            :style {:display (if (:open? dropdown) "block" "none")
                    :left (str (:left dropdown) "px")
                    :top (str (:top dropdown) "px")}}
        (div {:class "triangle-with-shadow"})
        (om/build dropzone (select-keys props [:uploader]))
        (let [files-count (count (:files uploader))]
          (if-not (zero? files-count)
            (button {:style {:width "70px"
                             :height "30px"
                             :position "absolute"
                             :bottom "86px"
                             :left "50%"
                             :margin-left "-35px"}
                     :on-click #(when-not (zero? files-count)
                                  (start-upload (select-keys props [:uploader
                                                                    :library
                                                                    :media])))}
                    "Start")))))))
