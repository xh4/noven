(ns noven.dashboard.editor
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require [noven.dashboard.appstate :refer [app-state]]
            [noven.dashboard.dropline :refer [dropline]]
            [noven.dashboard.util :refer [index-of vec-contains? find-index]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros
             [div span ul ol li a p img h6 h5 h4 h3 h2 h1
              textarea hr strong figure input br]]
            [domina :refer [set-html! has-class? attr
                            add-class! remove-class!]]
            [ajax.core :refer [GET POST PUT DELETE]]
            [cljs.core.async :refer [chan <!]]
            [clojure.string :as string]
            [clojure.set :refer [union difference intersection]]
            [clojure.zip :as z]
            [goog.dom :as dom]
            [goog.dom.Range]
            [goog.json :as json]
            [goog.style :as style]
            [goog.editor.ContentEditableField :as editor]
            [goog.events :as events]
            [goog.events.FileDropHandler]))

(declare field)

(def document (atom nil))

(defn enter-key? [e]
  (or (and (.-ctrlKey e)
           (= (.-keyCode e) (.. goog.events -KeyCodes -M)))
      (= (.-keyCode e) (.. goog.events -KeyCodes -ENTER))))

(defn delete-key? [e]
  (or (= (.-keyCode e) (.. goog.events -KeyCodes -DELETE))
      (= (.-keyCode e) (.. goog.events -KeyCodes -BACKSPACE))))

(defn space-key? [e]
  (= (.-keyCode e) (.. goog.events -KeyCodes -SPACE)))

(defn whitespace? [char]
  (when char
    (let [code (.charCodeAt char 0)]
      (or (= code 160)
          (= code 32)))))

(defn get-block [blocks sym]
  (first (filter #(= (:sym %) sym) blocks)))

(defn parallel-match [col1 col2 pred]
  (loop [col1 col1
         col2 col2]
    (when (and (not (empty? col1))
               (not (empty? col2)))
      (if (pred (first col1))
        (first col2)
        (recur (rest col1)
               (rest col2))))))

(defn previous-block [blocks sym]
  (parallel-match
   blocks
   (-> blocks butlast (conj nil))
   (fn [block]
     (= (:sym block) sym))))

(defn next-block [blocks sym]
  (parallel-match
   blocks
   (-> blocks rest vec (conj nil))
   (fn [block]
     (= (:sym block) sym))))

(defn make-splits [block]
  (let [markup-ranges (vals (:markups block))
        anchor-ranges (map :range (:anchors block))
        only-numbers (fn [col] (filter number? col))]
    (-> markup-ranges
        (conj anchor-ranges)
        flatten
        only-numbers
        (conj 0)
        (conj (count (:text block)))
        distinct
        sort
        vec)))

(defn make-ranges [splits]
  (let [divided (map (fn [itm]
                       (if (or (= (first splits) itm)
                               (= (last splits) itm)) itm [itm itm]))
                     splits)]
    (->> divided
         (flatten)
         (partition 2))))

(defn make-slices [text ranges]
  (mapv (fn [range] {:type :text
                    :range (vec range)
                    :content (apply subs text range)}) ranges))

(defn in-range? [range [start end]]
  (and (>= start (first range))
       (<= end (second range))))

(defn in-ranges? [ranges [start end]]
  (some #(in-range? % [start end]) ranges))

(defn offset-in-range? [[start end] offset]
  (and (>= offset start)
       (<= offset end)))

(defn wrap-em [block {:keys [range content] :as node}]
  (let [em-ranges (get-in block [:markups :em])]
    (if (in-ranges? em-ranges range)
      {:type :em
       :range range
       :content [node]}
      node)))

(defn wrap-strong [block {:keys [type range content] :as node}]
  (let [strong-ranges (get-in block [:markups :strong])]
    (if (in-ranges? strong-ranges range)
      {:type :strong
       :range range
       :content [node]}
      node)))

(defn wrap-a [block {:keys [type range content] :as node}]
  (let [anchors (:anchors block)]
    (reduce (fn [node anchor]
              (let [anchor-range (:range anchor)
                    url (:url anchor)]
                (if (in-range? anchor-range range)
                  {:type :a
                   :range range
                   :url url
                   :content [node]}
                  node))) node anchors)))

(defn wrap-slices [p slices]
  {:content (map #(->> %
                       (wrap-em p)
                       (wrap-strong p))
                 slices)})

(def content-zipper (partial z/zipper map?
                             (fn [node]
                               (cond
                                 (= (:type node) :text) nil
                                 :else (seq (:content node))))
                             (fn [node children] (assoc node :content (vec children)))))

(defn merge-nodes [l r]
  (-> l
      (assoc :range [(first (:range l))
                     (second (:range r))])
      (assoc :content (cond
                        (= (:type l) :text) (str (:content l) (:content r))
                        :else (apply conj (:content l) (:content r))))))

(defn merge-content [content]
  (reduce
   (fn [col {:keys [type content] :as node}]
     (let [prev (last col)]
       (if (= type (:type prev))
         (assoc col (dec (count col)) (merge-nodes prev node))
         (conj col node))))
   []
   content))

(defn zip-loc [loc]
  (if (z/end? loc)
    (z/node loc)
    (let [node (z/node loc)
          content (:content node)
          merged (if (string? content)
                   content
                   (merge-content content))]
      (-> loc
          (z/edit assoc :content merged)
          z/next
          zip-loc))))

(defn zip-content [content]
  (let [zp (content-zipper content)]
    (zip-loc zp)))

(defn render-content [content]
  (->> (map (fn [{:keys [type content] :as node}]
              (cond
                (= type :text) content
                (= type :strong) (str "<strong>" (render-content content) "</strong>")
                (= type :em) (str "<em>" (render-content content) "</em>")
                (= type :a) (str "<a href=\"" (:url node) "\">" (render-content content) "</a>")))
            content)
       (flatten)
       (apply str)))

(defn wrap-block [block]
  (let [tag (condp = (:type block)
              "paragraph" "p"
              "list-item" "li"
              "figure" "figure")
        attrs (conj {:data-sym (name (:sym block))}
                    (if (= (:type block) "figure") [:contentEditable false]))]
    (dom/createDom tag (clj->js attrs))))

(defn clean-text [s]
  (let [length (.-length s)]
    (if (whitespace? (nth s (dec length)))
      (str (.slice s 0 (dec length)) (.fromCharCode js/String 160)) s)))

(defn handle-empty-text [s]
  (if (= s "")
    "<br>"
    s))

(defn sanitize-text [s]
  (-> s
      clean-text
      handle-empty-text))

(defn create-block-element [block inner-html]
  )

(defmulti render-block
  (fn [block] (:type block)))

(defmethod render-block "paragraph" [block]
  (let [text (sanitize-text (:text block))
        splits (make-splits block)
        ranges (make-ranges splits)
        slices (make-slices text ranges)
        nodes (map #(->> %
                         (wrap-em block)
                         (wrap-strong block)
                         (wrap-a block)) slices)
        content {:content nodes :type :strong}
        zipped (zip-content content)
        block-node (wrap-block block)]
    (set! (. block-node -innerHTML) (render-content (:content zipped)))
    block-node))

(defmethod render-block "list-item" [block]
  (let [block-node (wrap-block block)]
    (set! (. block-node -innerHTML) (sanitize-text (:text block)))
    block-node))

(defmethod render-block "list" [block]
  (let [ul (dom/createDom "ul")
        items (map render-block (:items block))]
    (doseq [item items]
      (dom/appendChild ul item))
    ul))

(defmethod render-block "figure" [block]
  (let [block-node (wrap-block block)
        size (:size block)
        style (str "max-width:" (first size) "px;"
                   "max-height:" (second size) "px;"
                   "padding-bottom:" (second size) "px")
        wrapper (dom/createDom "div" (clj->js {:class "image-wrapper"
                                               :style style}))
        figcaption (dom/createDom "figcaption" (clj->js {:contentEditable "true"}))
        img (dom/createDom "img")]
    (set! (.-src img) (:img block))
    (set! (. figcaption -innerHTML) "caption")
    (dom/appendChild wrapper img)
    (dom/appendChild block-node wrapper)
    (dom/appendChild block-node figcaption)
    block-node))

(defn text-node? [node]
  (= (.-nodeType node) (.. goog.dom -NodeType -TEXT)))

(defn anchor-node? [node]
  (= (.-tagName node) (.. goog.dom -TagName -A)))

(defn strong-node? [node]
  (= (.-tagName node) (.. goog.dom -TagName -STRONG)))

(defn em-node? [node]
  (= (.-tagName node) (.. goog.dom -TagName -EM)))

(defn li-node? [node]
  (= (.-tagName node) (.. goog.dom -TagName -LI)))

(defn block-node? [node]
  (and (= (.-nodeType node) (.. goog.dom -NodeType -ELEMENT))
       (.getAttribute node "data-sym")))

(defn find-nodes-reverse [beg f]
  (if (f beg) beg
      (if-let [parent (dom/getParentElement beg)]
        (find-nodes-reverse parent))))

(defn find-node-reverse [beg f]
  (if-let [rv (find-nodes-reverse beg f)]
    (first rv)))

(defn find-block [focus-node]
  (if (block-node? focus-node)
    focus-node
    (if-let [parent (dom/getParentElement focus-node)]
      (find-block parent))))

(defn text-nodes [block-node]
  (dom/findNodes block-node text-node?))

(defn before-text-nodes [block text-node]
  (let [nodes (text-nodes block)
        index (index-of nodes text-node)]
    (first (split-at index nodes))))

(defn before-text-string [block text-node]
  (reduce str (map #(.-textContent %) (before-text-nodes block text-node))))

(defn update-block
  ([blocks block] (update-block blocks (:sym block) block))
  ([blocks sym block]
   (vec (map #(if (= (:sym %) sym) block %) blocks))))

(defn transact-block
  ([blocks sym f]
   (vec (map #(if (= (:sym %) sym) (f %) %) blocks))))

(defn delete-block [blocks sym]
  (reduce (fn [v b]
            (if (= (:sym b) sym)
              v
              (conj v b))) [] blocks))

(defn blocks-between-syms [blocks sym1 sym2]
  (let [syms (vec (map #(:sym %) blocks))
        sym1-idx (index-of syms sym1)
        sym2-idx (index-of syms sym2)]
    (->> (map (fn [idx sym]
                (if (and (> idx sym1-idx)
                         (< idx sym2-idx))
                  sym))
              (range (count syms))
              syms)
         (remove nil?))))

(defn block->el [sym]
  (dom/findNode (.-field field)
    (fn [node]
      (when-not (text-node? node)
        (= (attr node "data-sym") (name sym))))))

(defn get-block-node [sym]
  (dom/findNode (.-field field)
    (fn [node]
      (when-not (text-node? node)
        (= (attr node "data-sym") (name sym))))))

(defn HTMLCollection->vector [c]
  (for [idx (range 0 (.-length c))]
    (.item c idx)))

(defn field-children []
  (HTMLCollection->vector (dom/getChildren (.-field field))))

(defn block-nodes []
  (dom/findNodes (.-field field)
    (fn [node]
      (block-node? node))))

(defn block-sym [block-node]
  (keyword (attr block-node "data-sym")))

(defn selection-in-field? [field]
  (let [range (.getRange field)
        container (.getContainer range)
        field-node (.-field field)]
    (or (= field-node container)
        (dom/findNode field-node (fn [node] (= node container))))))

(defn collapse? [field]
  (let [range (.getRange field)]
    (if (selection-in-field? field)
      (.isCollapsed range))))

(defn caret? [field]
  (let [range (.getRange field)]
    (if (selection-in-field? field)
      (.isCollapsed range))))

(defn selection? [field]
  (let [range (.getRange field)]
    (if (selection-in-field? field)
      (not (.isCollapsed range)))))

(defn get-block-selection [range block-node]
  (let [block-range (goog.dom.Range/createFromNodeContents block-node)
        text-length (count (.-textContent block-node))
        start-node (.getStartNode range)
        start-offset (.getStartOffset range)
        end-node (.getEndNode range)
        end-offset (.getEndOffset range)
        start-before (count (before-text-string block-node start-node))
        end-before (count (before-text-string block-node end-node))
        range-in-block? (.containsRange block-range range)
        block-in-range? (.containsRange range block-range)
        start-in-block? (.containsNode block-range (.getStartNode range))
        end-in-block? (.containsNode block-range (.getEndNode range))
        sel-range (cond
                    block-in-range? [0 text-length]
                    range-in-block? [(+ start-before start-offset)
                                     (+ end-before end-offset)]
                    start-in-block? [(+ start-before start-offset)
                                     text-length]
                    end-in-block? [0 (+ end-before end-offset)])
        boundary (cond
                   start-in-block? {:text-node start-node
                                    :text-node-offset start-offset
                                    :offset (+ start-before start-offset)}
                   end-in-block? {:text-node end-node
                                  :text-node-offset end-offset
                                  :offset (+ end-before end-offset)})]
    (conj {:sym (block-sym block-node)
           :range sel-range
           :block-node block-node
           :all? (= sel-range [0 text-length])} boundary)))

(defn get-selection []
  (let [range (.getRange field)
        block-nodes (block-nodes)]
    (if (selection-in-field? field)
      (map #(get-block-selection range %) block-nodes)
      (.error js/console "Selection not in field."))))

(defn narrow-selection []
  (let [selection (get-selection)
        narrow (remove #(not (:range %)) selection)]
    {:start-block-sym (:sym (first narrow))
     :start-offset (first (:range (first narrow)))
     :end-block-sym (:sym (last narrow))
     :end-offset (last (:range (last narrow)))}))

(defn get-range []
  (let [selection (get-selection)
        narrow (remove #(not (:range %)) selection)]
    {:start-block-sym (:sym (first narrow))
     :start-offset (first (:range (first narrow)))
     :end-block-sym (:sym (last narrow))
     :end-offset (last (:range (last narrow)))}))

(defn get-caret []
  (let [selection (get-selection)
        narrow (remove #(not (:range %)) selection)]
    (first narrow)))

(defn get-offset []
  (:offset (get-caret)))

(defn range->set [[start end]]
  (set (range start end)))

(defn ranges->set [ranges]
  (apply union (map range->set ranges)))

(defn sequential-seqs
  ([] [])
  ([a] a)
  ([a b] (let [n (last (last a))]
           (if (and n (= (inc n) b))
             (update-in a [(dec (count a))] conj b)
             (conj a [b])))))

(defn set->ranges [s]
  (let [v (sort (distinct (vec s)))
        seqs (reduce sequential-seqs [] v)]
    (vec (map (fn [s] [(apply min s) (inc (apply max s))]) seqs))))

(defn add-range [ranges range]
  (let [orig (ranges->set ranges)
        new (range->set range)
        u (union orig new)]
    (set->ranges u)))

(defn remove-range [ranges range]
  (let [orig (ranges->set ranges)
        new (range->set range)
        u (difference orig new)]
    (set->ranges u)))

(defn union-ranges [ranges]
  (set->ranges (ranges->set ranges)))

(defn text-node-styles
  ([node] (text-node-styles node []))
  ([node styles]
   (if-let [parent (dom/getParentElement node)]
     (cond
       (block-node? node) styles
       (em-node? node) (text-node-styles parent (conj styles :em))
       (anchor-node? node) (text-node-styles parent (conj styles [:a (.getAttribute node "href")]))
       (strong-node? node) (text-node-styles parent (conj styles :strong))
       (text-node? node) (text-node-styles parent styles)))))

(defn text-slices [block]
  (let [nodes (text-nodes block)]
    (reduce (fn [col node]
              (let [lst (last col)
                    offset (second (:range lst))
                    length (.-length (.-textContent node))
                    range (if lst
                            [offset (+ offset length)]
                            [0 length])
                    styles (text-node-styles node)
                    markups (vec (map #(if (vector? %) (first %) %) styles))
                    anchor (second (first (filter vector? styles)))
                    m {:node node
                       :content (.-textContent node)
                       :markups markups
                       :anchor (if anchor {:range range :url anchor})
                       :range range}]
                (conj col m))) [] nodes)))

(defn restore-selection [{:keys [start-block-sym
                                 start-offset
                                 end-block-sym
                                 end-offset]}]
  (let [start-block (block->el start-block-sym)
        start-texts (text-slices start-block)
        start-text (first (filter #(offset-in-range? (:range %) start-offset) start-texts))
        start-node (or (:node start-text) start-block)
        start-node-offset (if start-text (- start-offset (first (:range start-text))) 0)

        end-block (block->el end-block-sym)
        end-texts (text-slices end-block)
        end-text (first (filter #(offset-in-range? (:range %) end-offset) end-texts))
        end-node (or (:node end-text) end-block)
        end-node-offset (if end-text (- end-offset (first (:range end-text))) 0)]
    (let [range (goog.dom.Range/createFromNodes
                 start-node
                 start-node-offset
                 end-node
                 end-node-offset)]
      (.select range))))

(defn reduce-anchors [anchors]
  (reduce (fn [v anchor]
            (let [lst (last v)]
              (cond
                (not lst) [anchor]
                (not= (first (:range anchor)) (second (:range lst))) (conj v anchor)
                :else (assoc-in v [(dec (count v)) :range 1 ] (second (:range anchor)))))) [] anchors))

(defn build-paragraph [slices]
  {:text (reduce str (map :content slices))
   :markups {:strong (->> (map #(if (vec-contains? (:markups %) :strong) (:range %)) slices)
                          (remove nil?)
                          (vec)
                          (union-ranges))
             :em (->> (map #(if (vec-contains? (:markups %) :em) (:range %)) slices)
                      (remove nil?)
                      (vec)
                      (union-ranges))}
   :anchors (->> (map :anchor slices)
                 (remove nil?)
                 (reduce-anchors))})

(defn map-back [block-node]
  (let [blocks (:blocks @document)
        sym (block-sym block-node)
        slices (text-slices block-node)
        p (build-paragraph slices)]
    (swap! document assoc :blocks (transact-block blocks sym #(merge % p)))))

(defn split-string [s offset]
  (map #(apply str %) (split-at offset (seq s))))

(defn split-ranges [ranges offset]
  (let [s (ranges->set ranges)]
    [(set->ranges (set (filter #(< % offset) s)))
     (set->ranges (set (filter #(>= % offset) s)))]))

(defn cut-ranges [ranges offset]
  (let [splited (split-ranges ranges offset)]
    [(first splited)
     (mapv (fn [v] [(- (first v) offset)
                   (- (second v) offset)]) (second splited))]))

(defn split-anchors [anchors offset]
  (let [ranges (mapv :range anchors)
        [left right] (cut-ranges ranges offset)
        left (mapv (fn [range]
                     (let [anchor (first (filter #(in-range? (:range %) range) anchors))]
                       {:range range
                        :url (:url anchor)})) left)
        right (mapv (fn [range]
                      (let [anchor (first (filter #(in-range? (:range %)
                                                              [(+ (first range) offset)
                                                               (+ (second range) offset)]) anchors))]
                        {:range range
                         :url (:url anchor)})) right)]
    [left right]))

(defn split-paragraph [block offset]
  (let [text (:text block)
        strong (get-in block [:markups :strong])
        em (get-in block [:markups :em])
        anchors (:anchors block)]
    [(merge block {:text (first (split-string text offset))
                   :markups {:strong (first (cut-ranges strong offset))
                             :em (first (cut-ranges em offset))}
                   :anchors (first (split-anchors anchors offset))})
     (merge block {:sym (keyword (gensym))
                   :text (second (split-string text offset))
                   :markups {:strong (second (cut-ranges strong offset))
                             :em (second (cut-ranges em offset))}
                   :anchors (second (split-anchors anchors offset))})]))

(defn cut-string [s [start end]]
  (let [s (reduce str (map-indexed (fn [idx char] (if (or (< idx start)
                                                         (>= idx end)) char)) s))]
    (if (nil? s) "" s)))

(defn cut-anchor [anchors [cut-start cut-end :as cut-range]]
  (let [new-ranges (map (fn [anchor]
                          (let [range (:range anchor)
                                start (first range)
                                end (second range)
                                cut-length (- cut-end cut-start)
                                is (intersection (range->set range)
                                                 (range->set cut-range))]
                            (cond
                              (<= cut-end start) [(- start cut-length) (- end cut-length)]
                              (>= cut-start end) range
                              (and (<= cut-start start) (>= cut-end end)) nil
                              (< cut-start start) [cut-start (- end cut-length)]
                              (>= cut-start start) [start (- end (count is))]))) anchors)]
    (->> (map
          (fn [anchor new-range]
            (if-not new-range
              nil
              (assoc anchor :range new-range)))
          anchors
          new-ranges)
         (remove nil?)
         (vec))))

(defn empty-block? [block]
  (zero? (count (:text block))))

(defn cut-paragraph [block range]
  (let [all? (and (= (first range) 0)
                  (= (second range) (count (:text block))))
        range (if (and (>= (first range) 1)
                       (whitespace? (nth (:text block) (first range)))
                       (whitespace? (nth (:text block) (dec (first range)))))
                [(dec (first range))
                 (second range)]
                range)
        strong (get-in block [:markups :strong])
        em (get-in block [:markups :em])
        anchors (:anchors block)]
    (merge block {:text (sanitize-text (cut-string (:text block) range))
                  :markups {:strong (remove-range strong range)
                            :em (remove-range em range)}
                  :anchors (cut-anchor anchors range)})))

(defn merge-paragraph [block other]
  (let [anchors (:anchors block)]
    (merge block {:text (str (:text block) (:text other))
                  :markups {:strong (union-ranges (concat (get-in block [:markups :strong])
                                                          (get-in other [:markups :strong])))
                            :em (union-ranges (concat (get-in block [:markups :em])
                                                      (get-in other [:markups :em])))}
                  :anchors (concat (:anchors block) (map (fn [anchor]
                                                           (let [range (:range anchor)
                                                                 length (count (:text block))]
                                                             (assoc anchor :range [(+ (first range) length)
                                                                                   (+ (second range) length)]))) anchors))})))

(defn zip-lists [blocks]
  (reduce (fn [col itm]
            (if (= (:type itm) "list-item")
              (if (= (:type (last col)) "list")
                (conj (vec (butlast col)) (update-in (last col) [:items] conj itm))
                (conj col {:type "list"
                           :items [itm]}))
              (conj col itm))) [] blocks))

(defn render-document []
  (let [blocks (zip-lists (:blocks @document))
        block-nodes (map render-block blocks)]
    (aset (dom/$ "document-title") "innerHTML" (:title @document))
    (.setHtml field false "")
    (doseq [b block-nodes]
      (dom/appendChild (.-field field) b))))

(defn get-focus-block-node []
  (if-let [caret (get-caret)]
    (:block-node caret)))

(defn previous-block-node [sym]
  (let [nodes (block-nodes)]
    (parallel-match
     nodes
     (-> nodes butlast (conj nil))
     (fn [node]
       (= (block-sym node) sym)))))

(defn next-block-node [sym]
  (let [nodes (block-nodes)]
    (parallel-match
     nodes
     (-> nodes rest vec (conj nil))
     (fn [node]
       (= (block-sym node) sym)))))

(defn place-cursor-at-start [sym]
  (if-let [node (get-block-node sym)]
    (let [range (goog.dom.Range/createFromNodeContents node)]
      (.collapse range true)
      (.select range))))

(defn place-cursor-at-end [sym]
  (if-let [node (get-block-node sym)]
    (let [range (goog.dom.Range/createFromNodeContents node)]
      (.collapse range false)
      (.select range))))

(defn insert-block-node [after-sym node]
  (if after-sym
    (if-let [sibling (get-block-node after-sym)]
      (dom/insertSiblingAfter sibling node))
    (dom/appendChild field node)))

(defn update-block-node [sym node]
  (if-let [old-node (get-block-node sym)]
    (dom/replaceNode node old-node)))

(defn remove-block-node [sym]
  (when-let [node (get-block-node sym)]
    (if (li-node? node)
      (let [prev (dom/getPreviousElementSibling node)
            next (dom/getNextElementSibling node)]
        (if (and (not prev) (not next))
          (dom/removeNode (dom/getParentElement node)))))
    (dom/removeNode node)))

(defn insert-block [blocks position sym new-block]
  (vec (flatten (map (fn [block]
                       (if (= (:sym block) sym)
                         (if (= position :after)
                           [block new-block]
                           [new-block block])
                         block))
                     blocks))))

(defn anchor-in-selection? []
  (let [selection (get-selection)]
    (some
     (fn [sel]
       (let [range (:range sel)
             block (get-block (:blocks @document) (:sym sel))
             anchor-ranges (map :range (:anchors block))]
         (not (empty? (intersection (range->set range)
                                    (ranges->set anchor-ranges))))))
     selection)))

(defn empty-paragraph []
  {:type "paragraph"
   :sym (keyword (gensym))
   :text ""
   :markups {:em []
             :strong []}
   :anchors []})

(defn empty-document []
  {:title ""
   :blocks [(empty-paragraph)]})

(defn set-document [d]
  (reset! document d)
  (when (empty? (:blocks @document))
    (swap! document assoc :blocks [(empty-paragraph)])))

(defn get-document []
  @document)

(defn bold []
  (let [blocks (:blocks @document)
        selection (get-selection)
        range (get-range)
        do-bold (fn [block {:keys [range]}]
                  (let [ranges (get-in block [:markups :strong])]
                    (if (in-ranges? ranges range)
                      (assoc-in block [:markups :strong] (remove-range ranges range))
                      (assoc-in block [:markups :strong] (add-range ranges range)))))]
    (swap! document assoc :blocks (vec (map do-bold blocks selection)))
    (render-document)
    (restore-selection range)))

(defn italic []
  (let [blocks (:blocks @document)
        selection (get-selection)
        range (get-range)
        do-italic (fn [block {:keys [range]}]
                    (let [ranges (get-in block [:markups :em])]
                      (if (in-ranges? ranges range)
                        (assoc-in block [:markups :em] (remove-range ranges range))
                        (assoc-in block [:markups :em] (add-range ranges range)))))]
    (swap! document assoc :blocks (vec (map do-italic blocks selection)))
    (render-document)
    (restore-selection range)))

(def saved-selection (atom nil))
(defn save-selection []
  (let [selection (get-selection)]
    (when selection
      (reset! saved-selection selection))))

(defn link [url]
  ;; When there is anchor in selection, remove the entire anchor, else add the range to anchors
  (let [blocks (:blocks @document)
        selection @saved-selection
        do-anchor (fn [block {:keys [range]}]
                    (update-in block [:anchors] conj {:range range
                                                      :url url}))]
    (swap! document assoc :blocks (vec (map do-anchor blocks selection)))
    (render-document)))

(defn unlink []
  (let [blocks (mapv
                (fn [block sel]
                  (update-in block [:anchors]
                             (fn [anchors]
                               (reduce
                                (fn [col a]
                                  (if (empty? (intersection (range->set (:range a))
                                                            (range->set (:range sel))))
                                    (conj col a)
                                    col))
                                []
                                anchors))))
                (:blocks @document)
                (get-selection))]
    (swap! document assoc :blocks blocks)
    (render-document)))

(defn delete-selection []
  (let [blocks (:blocks @document)
        selection (get-selection)
        sel (get-range)
        cutted(vec (map cut-paragraph blocks (map :range selection)))
        start-block (get-block cutted (:start-block-sym sel))
        end-block (get-block cutted (:end-block-sym sel))
        new-blocks (if (= (:sym start-block) (:sym end-block))
                     (vec (remove nil? cutted))
                     (-> cutted
                         (update-block (:sym start-block) (merge-paragraph start-block end-block))
                         (delete-block (:sym end-block))))]
    (swap! document assoc :blocks new-blocks)
    (render-document)
    (restore-selection (merge sel {:end-block-sym (:start-block-sym sel)
                                   :end-offset (:start-offset sel)}))))

(defn delete-backspace []
  (let [blocks (:blocks @document)
        sel (get-range)
        offset (get-offset)]
    (when (>= offset 1)
      (restore-selection (merge sel {:end-block-sym (:start-block-sym sel)
                                     :end-offset (dec offset)})))
    (when (= offset 0)
      (when-let [prev-block (previous-block blocks (:end-block-sym sel))]
        (restore-selection (merge sel {:start-block-sym (:sym prev-block)
                                       :start-offset (count (:text prev-block))}))))
    (delete-selection)))

(defn handle-enter []
  (let [range (.getRange field)
        focus-node (.getContainer range)
        block-el (find-block focus-node)
        sym (keyword (attr block-el "data-sym"))
        blocks (:blocks @document)
        block (get-block blocks sym)
        selection (get-selection)
        offset (get-offset)

        splited (split-paragraph block offset)
        new-block (second splited)
        new-blocks (reduce (fn [blocks block]
                             (if (= (:sym block) sym)
                               (into blocks splited)
                               (conj blocks block))) [] blocks)]
    (swap! document assoc :blocks (vec new-blocks))
    (render-document)
    (restore-selection {:start-block-sym (:sym new-block)
                        :start-offset 0
                        :end-block-sym (:sym new-block)
                        :end-offset 0})))

(defn on-keydown [e]
  (cond
    (enter-key? e)
    (do
      (.preventDefault e)
      (handle-enter))

    (delete-key? e)
    (do
      (.preventDefault e)
      (if (caret? field)
        (delete-backspace)
        (delete-selection)))

    ;; 1. Prevent press whitespace at the beginning of a paragraph
    ;; 2. Prevent press whitespace when the character before or after cursor is whitespace
    (space-key? e)
    (let [caret (get-caret)
          node (:block-node caret)
          text (.-textContent node)
          offset (:offset caret)]
      (when (or (whitespace? (nth text (dec offset)))
                (whitespace? (nth text offset)))
        (.preventDefault e)))

    ;; When press a key, if something is currently selected, first delete the content of selection
    (selection? field)
    (do
      (delete-selection))))

(defn on-keyup [e]
  ;; When we type something, 'map back' block node into our inner block representation
  (let [range (.getRange field)
        focus-node (.getContainer range)
        block-node (find-block focus-node)]
    (when (and block-node
               (not (enter-key? e))
               (not= (.-keyCode e) 0))
      (map-back block-node))))

(defn on-click
  [e]
  ;; Add an `selected` class for current focusing block node.
  (let [y (.-clientY e)
        children (field-children)
        margin 28
        outlines (map (fn [node]
                        (let [coordinate (goog.style/getPageOffset node)
                              above-line (.-y coordinate)
                              bottom-line (+ above-line
                                             (.-height (goog.style/getSize node)))]
                          [above-line bottom-line node])) children)]
    (doseq [[above-line bottom-line block-el] outlines]
      (let [current? (and (>= y above-line)
                          (<= y bottom-line))]
        (if current?
          (add-class! block-el "selected")
          (remove-class! block-el "selected"))
        (when current?
          ;; when focus on figure
          (when (= (.-tagName block-el) "FIGURE")
            (let [range (.getRange field)]
              (when range
                (let [caption-node (dom/findNode block-el
                                     (fn [node]
                                       (and
                                        (dom/isElement node)
                                        (= (.-tagName node) "FIGCAPTION"))))
                      text-node (.getContainer range)]
                  (when-not (goog.dom/contains caption-node text-node)
                    (goog.dom.Range/clearSelection)))))))))))

(defn attach-field-events []
  (.removeAllListeners field)
  (.addListener field "compositionstart" (fn [e] ))
  (.addListener field "compositionend" (fn [e] ))
  (.addListener field "click" on-click)
  (.addListener field "keydown" on-keydown)
  (.addListener field "keyup" on-keyup)
  (.addListener field "dragover" (fn [e] ))
  (.addListener field "dragleave" (fn [e] )))

(defcomponent editor-toolbar [toolbar owner]
  (render-state [_ {:keys [channel]}]
    (let [text-button (fn [{:keys [title class on-click]}]
                        (li {:style {:display (if (= (:mode toolbar) :text) "block" "none")}
                             :on-click on-click}
                            (a {:href "javascript:void(0);"
                                :title title
                                :class class})))
          media-button (fn [{:keys [title class]}]
                         (li {:style {:display (if (= (:mode toolbar) :media) "block" "none")}}
                             (a {:href "javascript:void(0);"
                                 :title title
                                 :class class})))
          separator (fn []
                      (li {:class "redactor_separator"
                           :style {:display (if (= (:mode toolbar) :text) "block" "none")}}))]
      (div {:class (str "redactor_air")
            :style {:left (str (:left toolbar) "px")
                    :top (str (:top toolbar) "px")
                    :display (if (:display toolbar) "block" "none")}}
        (div {:class "redactor_toolbar"}
          (if (= (:mode toolbar) :link)
            (span {:style {:display "block"}}
                  (input {:placeholder "Paste or type a link"
                          :style {:height "32px"
                                  :line-height "32px"
                                  :font-size "12px"
                                  :background "none"
                                  :border "0"}
                          :ref "url-input"
                          :on-mouse-over #(save-selection)
                          :on-key-up (fn [e]
                                       (when (= (.-keyCode e) (.. goog.events -KeyCodes -ENTER))
                                         (om/update! toolbar :mode :text)
                                         (link (.. e -currentTarget -value))))})
                  (span {:style {:height "32px"
                                 :line-height "32px"
                                 :font-size "12px"
                                 :display "inline-block"
                                 :text-align "center"
                                 :width "16px"
                                 :float "right"
                                 :cursor "pointer"}
                         :on-click (fn [e]
                                     (om/update! toolbar :mode :text))} "×"))
            (ul {:class "redactor_buttons"
                 :style {:padding 0}}
                (text-button {:title "Bold"
                              :class "redactor_btn_bold"
                              :on-click #(bold)})
                (text-button {:title "Italic"
                              :class "redactor_btn_italic"
                              :on-click #(italic)})
                (separator)
                (text-button {:title "Add link"
                              :class "redactor_btn_klink"
                              :on-click #(if (anchor-in-selection?)
                                           (unlink)
                                           (om/update! toolbar :mode :link))})
                (media-button {:title "Insert library photo"
                               :class "redactor_btn_kshortcodekoken_photo"})
                (media-button {:title "Insert video"
                               :class "redactor_btn_kshortcodekoken_video"})
                (media-button {:title "Insert slideshow"
                               :class "redactor_btn_kshortcodekoken_slideshow"})
                (media-button {:title "Upload and insert image"
                               :class "redactor_btn_kshortcodekoken_upload"})
                (media-button {:title "Insert costom code"
                               :class "redactor_btn_kshortcodekoken_code"})
                (media-button {:title "Insert read more"
                               :class "redactor_btn_kshortcoderead_more"})))))))
  (did-update [_ next-props next-state]
    (if (and (= (:mode next-props) :link)
             (not (= (.-activeElement js/document)
                     (om/get-node owner "url-input"))))
      (.focus (om/get-node owner "url-input")))))

(defcomponent document-title [{:keys [editor]} owner]
  (init-state [_]
    {:composition false})
  (render [_]
    (div {:class "document-title"}
      (div {:class "document-title-wrap"}
        (h2 {:contentEditable true
             :id "document-title"
             :ref "title"
             :data-placeholder "Title"
             :on-key-up (fn [e]
                          (when-not (om/get-state owner :composition)
                            (swap! document assoc :title (dom/getTextContent (om/get-node owner "title")))))
             :on-compositionStart #(om/set-state! owner :composition true)
             :on-compositionEnd #(om/set-state! owner :composition false)})))))

(defcomponent editor-view [{:keys [editor]} owner]
  (init-state [_]
    {:droplines (atom [])
     :toolbar-channel (chan)})
  (render-state [_ state]
    (div {:id "editor"}
      (div {:class "document"}
        (om/build document-title editor)
        (div {:class "document-blocks"
              :contentEditable true
              :id "doc"
              :ref "document"}))
      (om/build editor-toolbar (:toolbar editor)
                {:init-state {:channel (:toolbar-channel state)}})))
  (did-mount [_]
    (let [field (goog.editor.ContentEditableField. "doc")
          field-node (om/get-node owner "document")]
      (def field field)
      (.makeEditable field)
      (attach-field-events)
      (render-document))

    (go-loop []
      (let [v (<! (om/get-state owner :toolbar-channel))]
        (cond
          (= v :bold) (bold)
          (= v :italic) (italic)
          (= v :save-selection) (save-selection)
          (= (:message v) :anchor) (link (:url v))
          :else nil)
        (recur)))))
