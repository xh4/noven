(ns noven.dashboard
  (:require [noven.dashboard.appstate :refer [app-state]]
            [noven.dashboard.app :refer [app-view]]
            [noven.dashboard.login :refer [login-view]]
            [noven.dashboard.ui :refer [ui-view]]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponent]]
            [om-tools.dom :refer-macros [h1]]
            [goog.dom :as dom]
            [goog.events :as events]
            [goog.history.EventType :as EventType]
            [secretary.core :as secretary :refer-macros [defroute]])
  (:import [goog History]))

(secretary/set-config! :prefix "#")

(defonce history (History.))

(defroute "/" {:as params}
  (swap! app-state assoc-in [:interface] :library)
  (swap! app-state assoc-in [:library :tab] :timeline))

(defn toggle-tab [name & [location]]
  (let [current-tab (get-in @app-state [:library :tab])
        next-tab (if location
                   (keyword (str name "#" location))
                   (keyword name))]
    (swap! app-state update-in [:library] merge {:previous-tab current-tab
                                                 :tab next-tab
                                                 :selected []})))

(defroute "/library" {:as params}
  (toggle-tab "timeline"))

(defroute "/library/:tab" {:keys [tab] :as params}
  (toggle-tab tab))

(defroute "/library/:tab/*" {:keys [tab *] :as params}
  (toggle-tab tab *))

(defroute "/text" {:as params}
  (swap! app-state assoc-in [:interface] :text))

(defroute "/site" {:as params}
  (swap! app-state assoc-in [:interface] :site))

(extend-type js/HTMLCollection
  ISeqable
  (-seq [array] (array-seq array 0))

  ICounted
  (-count [a] (alength a))

  IIndexed
  (-nth
    ([array n]
     (if (< n (alength array)) (aget array n)))
    ([array n not-found]
     (if (< n (alength array)) (aget array n)
         not-found)))

  ILookup
  (-lookup
    ([array k]
     (aget array k))
    ([array k not-found]
     (-nth array k not-found)))

  IReduce
  (-reduce
    ([array f]
     (ci-reduce array f))
    ([array f start]
     (ci-reduce array f start))))

(extend-type js/NodeList
  ISeqable
  (-seq [array] (array-seq array 0))

  ICounted
  (-count [a] (alength a))

  IIndexed
  (-nth
    ([array n]
     (if (< n (alength array)) (aget array n)))
    ([array n not-found]
     (if (< n (alength array)) (aget array n)
         not-found)))

  ILookup
  (-lookup
    ([array k]
     (aget array k))
    ([array k not-found]
     (-nth array k not-found)))

  IReduce
  (-reduce
    ([array f]
     (ci-reduce array f))
    ([array f start]
     (ci-reduce array f start))))

(defcomponent dashboard-view [app owner]
  (render [_]
    (let [logged-in? (get-in app [:site :logged-in?])]
      (when-not (nil? logged-in?)
        (om/build (if logged-in? app-view login-view) app))))
  (will-unmount [_]
    (events/removeAll js/window "resize")))

(defn main []
  (events/removeAll history)
  (events/listen history EventType/NAVIGATE #(secretary/dispatch! (.-token %)))
  (doto history (.setEnabled true))
  (om/root dashboard-view app-state
           {:target (. js/document (getElementById "container"))}))
