(ns noven.test
  (:require [noven.core :as core]
            [taoensso.timbre :as timbre])
  (:gen-class))

(defn -main [& args]
  (timbre/set-config! [:appenders :standard-out :enabled?] false)
  (timbre/set-config! [:appenders :spit :enabled?] true)
  (apply core/main args))
