(ns noven.dev
  (:require [noven.core :as core]
            [taoensso.timbre :as timbre]
            [cemerick.piggieback :as piggieback]
            [weasel.repl.websocket :as weasel]
            [figwheel-sidecar.auto-builder :as figwheel-auto]
            [figwheel-sidecar.core :as figwheel])
  (:gen-class))

(defn browser-repl []
  (let [repl-env (weasel/repl-env :ip "0.0.0.0" :port 4007)]
    (piggieback/cljs-repl :repl-env repl-env)
    (piggieback/cljs-eval repl-env '(in-ns 'noven.dashboard) {})))

(defn start-figwheel []
  (let [server (figwheel/start-server {:css-dirs ["resources/assets/stylesheets/dev"]
                                       :server-port 4004})
        config {:builds [{:id "dev"
                          :source-paths ["env/dev/cljs" "src/cljs"]
                          :compiler {:output-to            "resources/assets/javascripts/dev/dashboard.js"
                                     :output-dir           "resources/assets/javascripts/dev/out"
                                     :source-map           "resources/assets/javascripts/dev/dashboard.js.map"
                                     :source-map-timestamp true
                                     :preamble             ["resources/assets/vendor/react.js"]}}]
                :figwheel-server server}]
    (figwheel-auto/autobuild* config)))

(defn -main [& args]
  (timbre/set-config! [:appenders :standard-out :enabled?] false)
  (timbre/set-config! [:appenders :spit :enabled?] true)
  (apply core/main args))
