(ns noven.dev
  (:require [noven.dashboard :as dashboard]
            [figwheel.client :as figwheel :include-macros true]
            [weasel.repl :as weasel]
            [clojure.string :as string]))

(enable-console-print!)

(figwheel/watch-and-reload
 :websocket-url "ws://localhost:4004/figwheel-ws"
 :jsload-callback (fn [] (dashboard/main))
 :url-rewriter    (fn [url]
                    (-> url
                        (string/replace "//localhost:4004resources" "")
                        (string/replace "/stylesheets" "")
                        (string/replace "/javascripts" ""))))

(weasel/connect "ws://localhost:4007" :verbose true :print #{:repl :console})

(dashboard/main)
