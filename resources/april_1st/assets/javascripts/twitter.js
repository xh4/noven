// copyright Kevin Keen <kevin@coobii.com>
// 2015-06-10

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
};
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = 0, len = this.length; i < len; i++) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
};

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function onResize() {
    var figures = document.querySelectorAll(".tweet figure");
    var windowWidth = document.documentElement.clientWidth
            || document.body.clientWidth;

    var figureWidth = windowWidth <= 480 ? windowWidth * 0.75 : 480;

    for (var i = 0; i < figures.length; ++i) {
        var figure = figures[i],
            image = figure.querySelector("img"),
            width = parseInt(figure.getAttribute("data-image-width")),
            height = parseInt(figure.getAttribute("data-image-height"));
        var figureHeight = imageHeight < 260 ? imageHeight : 260,
            imageWidth = width < figureWidth ? width : figureWidth,
            imageHeight = width < figureWidth ? height : height * (imageWidth / width),
            top = (figureHeight - imageHeight) / 2.0,
            left = (figureWidth - imageWidth) / 2.0;
        figure.style.height = figureHeight + "px";
        figure.style.width = figureWidth + "px";
        image.style.top = top + "px";
        image.style.left = left + "px";
        image.style.width = imageWidth + "px";
        image.style.height = imageHeight + "px";
        // image.style.display = "block";
    }
};

var ImageLoader = function(spinner, image) {
    var timeout = false,
        loaded = false,
        showed = false;

    var showImage = function() {
        spinner.remove();
        image.style.display = "block";
    };

    setTimeout(function() {
        timeout = true;
        if (loaded && !showed) {
            showed = true;
            showImage();
        }
    }, getRandomInt(400, 1400));

    image.onload = function() {
        loaded = true;
        if(timeout && !showed) {
            showed = true;
            showImage();
        }
    };
};

function loadImage(figure) {
    var spinner = figure.querySelector(".spinner_pos"),
        image = figure.querySelector("img");

    new ImageLoader(spinner, image);

    image.setAttribute("src", image.getAttribute("data-src"));
};

function reveal () {
    for(var i = 0; i < lazyImages.length; i++)
    {
	var offsetParentTop = 0;
	var figure = lazyImages[i];
	do
	{
	    if(!isNaN(figure.offsetTop))
	    {
		offsetParentTop += figure.offsetTop;
	    }
	}while(figure = figure.offsetParent)

	var pageYOffset = window.pageYOffset;
	var viewportHeight = window.innerHeight;

	var offsetParentLeft = 0;
	var figure = lazyImages[i];
	do
	{
	    if(!isNaN(figure.offsetLeft))
	    {
		offsetParentLeft += figure.offsetLeft;
	    }
	}while(figure = figure.offsetParent);

	var pageXOffset = window.pageXOffset;
	var viewportWidth = window.innerWidth;

	if(offsetParentTop > pageYOffset && offsetParentTop < pageYOffset + viewportHeight && offsetParentLeft > pageXOffset && offsetParentLeft < pageXOffset + viewportWidth)
	{
            loadImage(lazyImages[i]);

	    lazyImages.splice(i, 1);
	    i--;
	}
	else
	{
	    /*console.log("offsetParentTop" + offsetParentTop + " pageYOffset" + pageYOffset + " viewportHeight" + window.innerHeight);*/
	}
    }
}

window.addEventListener("resize", onResize, false);
window.addEventListener("resize", reveal, false);
window.addEventListener("scroll", reveal, false);

document.addEventListener("DOMContentLoaded", function() {
    onResize();
    reveal();
}, false);
