// copyright Kevin Keen <kevin@coobii.com>
// 2015-06-10

var lazyImage = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAACXBIWXMAAAsTAAALEwEAmpwYAAAA\nDElEQVQI12P4//8/AAX+Av7czFnnAAAAAElFTkSuQmCC';

var lazyImages = [];

function createSpinner() {
    var spinner = document.createElement("div");
    spinner.className = "spinner_pos active";
    spinner.innerHTML = "<div class=\"spinner\"></div>";
    return spinner;
}

function lazyLoad() {
    var wrappers = document.querySelectorAll(".lazy-image[data-lazy='true']");
    for (var i = 0; i < wrappers.length; ++i) {
        var wrapper = wrappers[i],
            spinner = createSpinner(),
            image = wrapper.querySelector("img"),
            url = image.getAttribute("src");

        wrapper.setAttribute("data-lazy", "false");
        image.setAttribute("data-src", url);
        image.src = lazyImage;

        wrapper.appendChild(spinner);

        lazyImages.push(wrapper);
    }
}

var interval = setInterval(lazyLoad, 50);

document.addEventListener("DOMContentLoaded", function() {
    clearInterval(interval);
    lazyLoad();
}, false);
